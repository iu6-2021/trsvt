EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема электрическая\\nпринципиальная"
Date ""
Rev ""
Comp "МГТУ им. Н. Э. Баумана\\nГруппа ИУ6И-71Б"
Comment1 "Устройство для формирования\\n 4-х ШИМ-сигналов"
Comment2 "Фунг В. Х."
Comment3 "Хартов В. Я."
Comment4 ""
$EndDescr
$Comp
L Kur-Princ:Output XP4
U 1 1 61F2A9AD
P 9250 8000
F 0 "XP4" H 9150 8650 50  0000 L CNN
F 1 "Разъем выходов" H 8900 8550 50  0000 L CNN
F 2 "Principle:JST_B4B-XH-A(LF)(SN)" H 8800 8550 50  0001 C CNN
F 3 "" H 8800 8550 50  0001 C CNN
	1    9250 8000
	1    0    0    -1  
$EndComp
$Comp
L Kur-Princ:Power XP1
U 1 1 6224FC59
P 4000 8500
F 0 "XP1" H 4058 8965 50  0000 C CNN
F 1 "Разъем питания" H 4058 8874 50  0000 C CNN
F 2 "Connector:Banana_Jack_2Pin" H 3550 8850 50  0001 C CNN
F 3 "" H 3550 8850 50  0001 C CNN
	1    4000 8500
	1    0    0    -1  
$EndComp
$Comp
L Principle-rescue:+5V-power-Principle-rescue #PWR0107
U 1 1 62217BA5
P 6500 8200
F 0 "#PWR0107" H 6500 8050 50  0001 C CNN
F 1 "+5V" H 6515 8373 50  0000 C CNN
F 2 "" H 6500 8200 50  0001 C CNN
F 3 "" H 6500 8200 50  0001 C CNN
	1    6500 8200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 2450 6500 2600
$Comp
L Principle-rescue:+5V-power-Principle-rescue #PWR0113
U 1 1 61B4DD58
P 6500 2300
F 0 "#PWR0113" H 6500 2150 50  0001 C CNN
F 1 "+5V" H 6515 2473 50  0000 C CNN
F 2 "" H 6500 2300 50  0001 C CNN
F 3 "" H 6500 2300 50  0001 C CNN
	1    6500 2300
	1    0    0    -1  
$EndComp
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0112
U 1 1 61B4DD51
P 6500 6750
F 0 "#PWR0112" H 6500 6500 50  0001 C CNN
F 1 "GND" H 6505 6577 50  0000 C CNN
F 2 "" H 6500 6750 50  0001 C CNN
F 3 "" H 6500 6750 50  0001 C CNN
	1    6500 6750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6500 6600 6500 6750
Wire Wire Line
	5500 8750 5500 8900
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0108
U 1 1 6210B285
P 5500 8900
F 0 "#PWR0108" H 5500 8650 50  0001 C CNN
F 1 "GND" H 5505 8727 50  0000 C CNN
F 2 "" H 5500 8900 50  0001 C CNN
F 3 "" H 5500 8900 50  0001 C CNN
	1    5500 8900
	-1   0    0    -1  
$EndComp
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0116
U 1 1 620AE356
P 2500 4650
F 0 "#PWR0116" H 2500 4400 50  0001 C CNN
F 1 "GND" H 2505 4477 50  0000 C CNN
F 2 "" H 2500 4650 50  0001 C CNN
F 3 "" H 2500 4650 50  0001 C CNN
	1    2500 4650
	-1   0    0    -1  
$EndComp
NoConn ~ 7100 5900
NoConn ~ 7100 5800
Wire Wire Line
	7100 6200 7650 6200
Wire Wire Line
	7650 6100 7100 6100
Entry Wire Line
	7650 6200 7750 6300
Entry Wire Line
	7650 6100 7750 6200
Text Label 7300 6200 0    50   ~ 0
OUTPUT_2
Text Label 7300 6100 0    50   ~ 0
OUTPUT_1
Wire Wire Line
	8600 7650 7850 7650
Wire Wire Line
	7850 7750 8600 7750
Wire Wire Line
	8600 7850 7850 7850
Wire Wire Line
	7850 7950 8600 7950
Text Label 7850 7750 0    50   ~ 0
OUTPUT_2
Text Label 7850 7650 0    50   ~ 0
OUTPUT_1
Text Label 7850 7950 0    50   ~ 0
OUTPUT_4
Text Label 7850 7850 0    50   ~ 0
OUTPUT_3
Entry Wire Line
	7750 7850 7850 7950
Entry Wire Line
	7750 7750 7850 7850
Entry Wire Line
	7750 7650 7850 7750
Entry Wire Line
	7750 7550 7850 7650
Wire Wire Line
	5750 3300 5750 3700
Wire Wire Line
	3200 2500 3500 2500
Wire Wire Line
	3200 4500 3900 4500
Wire Wire Line
	3200 4000 3900 4000
Wire Wire Line
	3200 3500 3900 3500
Wire Wire Line
	3900 3000 3200 3000
NoConn ~ 5900 5800
NoConn ~ 5900 5700
NoConn ~ 5900 5600
NoConn ~ 7100 5400
NoConn ~ 7100 5300
NoConn ~ 7100 5200
NoConn ~ 7100 5100
NoConn ~ 7100 5000
NoConn ~ 7100 4900
NoConn ~ 7100 4800
NoConn ~ 7100 4700
NoConn ~ 7100 3600
NoConn ~ 7100 3500
NoConn ~ 7100 3400
NoConn ~ 7100 3300
$Comp
L Principle-rescue:SW_Push-Switch-Principle-rescue SW1
U 1 1 61784C66
P 3000 3000
F 0 "SW1" H 3000 3300 50  0000 C CNN
F 1 "Режим 1: 75 Гц 0.5 мс" H 3000 3194 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3000 3200 50  0001 C CNN
F 3 "~" H 3000 3200 50  0001 C CNN
	1    3000 3000
	-1   0    0    -1  
$EndComp
$Comp
L Principle-rescue:SW_Push-Switch-Principle-rescue SW2
U 1 1 617860C8
P 3000 3500
F 0 "SW2" H 3000 3800 50  0000 C CNN
F 1 "Режим 2: 75 Гц 2 мс" H 3000 3694 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3000 3700 50  0001 C CNN
F 3 "~" H 3000 3700 50  0001 C CNN
	1    3000 3500
	-1   0    0    -1  
$EndComp
$Comp
L Principle-rescue:SW_Push-Switch-Principle-rescue SW3
U 1 1 61786458
P 3000 4000
F 0 "SW3" H 3000 4300 50  0000 C CNN
F 1 "Режим 3: 200 Гц 0.5 мс" H 3000 4194 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3000 4200 50  0001 C CNN
F 3 "~" H 3000 4200 50  0001 C CNN
	1    3000 4000
	-1   0    0    -1  
$EndComp
$Comp
L Principle-rescue:SW_Push-Switch-Principle-rescue SW4
U 1 1 617866AD
P 3000 4500
F 0 "SW4" H 3000 4800 50  0000 C CNN
F 1 "Режим 4: 200 Гц 2 мс" H 3000 4694 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3000 4700 50  0001 C CNN
F 3 "~" H 3000 4700 50  0001 C CNN
	1    3000 4500
	-1   0    0    -1  
$EndComp
Entry Wire Line
	3900 3000 4000 3100
Entry Wire Line
	3900 3500 4000 3600
Entry Wire Line
	3900 4000 4000 4100
Entry Wire Line
	3900 4500 4000 4600
$Comp
L Principle-rescue:SW_Push-Switch-Principle-rescue SW5
U 1 1 619BD883
P 3000 2500
F 0 "SW5" H 3000 2800 50  0000 C CNN
F 1 "Reset" H 3000 2694 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 3000 2700 50  0001 C CNN
F 3 "~" H 3000 2700 50  0001 C CNN
	1    3000 2500
	-1   0    0    -1  
$EndComp
Entry Wire Line
	3900 2500 4000 2600
Wire Wire Line
	2800 3000 2500 3000
Wire Wire Line
	2500 2500 2500 3000
Wire Wire Line
	2500 2500 2800 2500
Wire Wire Line
	2800 4500 2500 4500
Connection ~ 2500 4000
Wire Wire Line
	2500 4000 2800 4000
Connection ~ 2500 3500
Wire Wire Line
	2500 3500 2500 4000
Wire Wire Line
	2800 3500 2500 3500
Connection ~ 2500 3000
Wire Wire Line
	2500 3000 2500 3500
Text Label 3700 2500 0    50   ~ 0
RESET
Text Label 3750 3000 0    50   ~ 0
SW1
Text Label 3750 3500 0    50   ~ 0
SW2
Text Label 3750 4000 0    50   ~ 0
SW3
Text Label 3750 4500 0    50   ~ 0
SW4
Entry Wire Line
	4000 2800 4100 2900
$Comp
L Principle-rescue:LM016L-Kur-Princ DD2
U 1 1 61B15254
P 10200 2500
F 0 "DD2" H 10850 3050 50  0000 L CNN
F 1 "LM016L" H 9500 3050 50  0000 L CNN
F 2 "Principle:LCD_LM016L" H 10950 3050 50  0001 C CNN
F 3 "" H 10950 3050 50  0001 C CNN
	1    10200 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 3350 5050 3300
Wire Wire Line
	5500 3700 5750 3700
Connection ~ 5500 3700
Wire Wire Line
	5500 3650 5500 3700
Connection ~ 5500 3300
Wire Wire Line
	5500 3300 5500 3350
Wire Wire Line
	4900 3300 5050 3300
Wire Wire Line
	5500 3100 5500 3300
Wire Wire Line
	4500 3300 4500 3700
Wire Wire Line
	4500 3700 4600 3700
Connection ~ 4500 3700
Wire Wire Line
	4500 3700 4500 3850
Wire Wire Line
	4600 3300 4500 3300
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0114
U 1 1 61B4DD7F
P 4500 3850
F 0 "#PWR0114" H 4500 3600 50  0001 C CNN
F 1 "GND" H 4505 3677 50  0000 C CNN
F 2 "" H 4500 3850 50  0001 C CNN
F 3 "" H 4500 3850 50  0001 C CNN
	1    4500 3850
	-1   0    0    -1  
$EndComp
$Comp
L Principle-rescue:C-Device-Principle-rescue C9
U 1 1 61B4DD79
P 4750 3700
F 0 "C9" V 5000 3650 50  0000 L CNN
F 1 "22п" V 4900 3600 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4788 3550 50  0001 C CNN
F 3 "~" H 4750 3700 50  0001 C CNN
	1    4750 3700
	0    -1   -1   0   
$EndComp
$Comp
L Principle-rescue:R-Device-Principle-rescue R2
U 1 1 61B4DD73
P 5500 3500
F 0 "R2" H 5600 3550 50  0000 L CNN
F 1 "220" H 5570 3455 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 5430 3500 50  0001 C CNN
F 3 "~" H 5500 3500 50  0001 C CNN
	1    5500 3500
	1    0    0    -1  
$EndComp
$Comp
L Principle-rescue:C-Device-Principle-rescue C8
U 1 1 61B4DD67
P 4750 3300
F 0 "C8" V 5000 3250 50  0000 L CNN
F 1 "22п" V 4900 3200 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4788 3150 50  0001 C CNN
F 3 "~" H 4750 3300 50  0001 C CNN
	1    4750 3300
	0    -1   -1   0   
$EndComp
Text Label 4100 2900 0    50   ~ 0
RESET
Text Label 7750 3200 0    50   ~ 0
SW4
Text Label 7750 3100 0    50   ~ 0
SW3
Text Label 7750 3000 0    50   ~ 0
SW2
Text Label 7750 2900 0    50   ~ 0
SW1
Entry Wire Line
	7900 3200 8000 3300
Entry Wire Line
	7900 3100 8000 3200
Entry Wire Line
	7900 3000 8000 3100
Entry Wire Line
	7900 2900 8000 3000
$Comp
L Principle-rescue:L7805-Kur-Princ DD4
U 1 1 61947CCD
P 5500 8350
F 0 "DD4" H 5500 8615 50  0000 C CNN
F 1 "L7805" H 5500 8524 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-218-3_Vertical" H 5525 8200 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 5500 8300 50  0001 C CNN
	1    5500 8350
	1    0    0    -1  
$EndComp
Connection ~ 5000 8750
Wire Wire Line
	5500 8750 5000 8750
Connection ~ 5000 8350
Wire Wire Line
	5200 8350 5000 8350
Wire Wire Line
	6000 8400 6000 8350
Wire Wire Line
	6000 8350 5800 8350
Wire Wire Line
	6000 8750 6000 8700
Wire Wire Line
	5500 8750 6000 8750
$Comp
L Principle-rescue:C-Device-Principle-rescue C2
U 1 1 619B1E8C
P 6000 8550
F 0 "C2" H 6100 8600 50  0000 L CNN
F 1 "100н" H 6115 8505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6038 8400 50  0001 C CNN
F 3 "~" H 6000 8550 50  0001 C CNN
	1    6000 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 8400 5000 8350
Wire Wire Line
	5000 8750 4600 8750
Wire Wire Line
	5000 8750 5000 8700
$Comp
L Principle-rescue:C-Device-Principle-rescue C1
U 1 1 6199C342
P 5000 8550
F 0 "C1" H 5150 8600 50  0000 L CNN
F 1 "330н" H 5115 8505 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 5038 8400 50  0001 C CNN
F 3 "~" H 5000 8550 50  0001 C CNN
	1    5000 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4600 8350 5000 8350
Wire Wire Line
	5500 8750 5500 8650
Connection ~ 5500 8750
Wire Wire Line
	4600 8750 4600 8450
Wire Wire Line
	9700 2850 9700 2900
$Comp
L Principle-rescue:+5V-power-Principle-rescue #PWR0109
U 1 1 6197B2D3
P 9350 2850
F 0 "#PWR0109" H 9350 2700 50  0001 C CNN
F 1 "+5V" H 9365 3023 50  0000 C CNN
F 2 "" H 9350 2850 50  0001 C CNN
F 3 "" H 9350 2850 50  0001 C CNN
	1    9350 2850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9600 2850 9600 3000
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0106
U 1 1 619720B2
P 9600 3000
F 0 "#PWR0106" H 9600 2750 50  0001 C CNN
F 1 "GND" H 9605 2827 50  0000 C CNN
F 2 "" H 9600 3000 50  0001 C CNN
F 3 "" H 9600 3000 50  0001 C CNN
	1    9600 3000
	-1   0    0    -1  
$EndComp
NoConn ~ 10550 2850
Text Label 10150 3350 1    50   ~ 0
E
Text Label 10050 3350 1    50   ~ 0
RW
Text Label 9950 3350 1    50   ~ 0
RS
Text Label 7300 6300 0    50   ~ 0
OUTPUT_4
Text Label 7300 6000 0    50   ~ 0
OUTPUT_3
NoConn ~ 9800 2850
NoConn ~ 10450 2850
NoConn ~ 10350 2850
NoConn ~ 10250 2850
Entry Wire Line
	10650 3400 10750 3500
Entry Wire Line
	10850 3400 10950 3500
Entry Wire Line
	10950 3400 11050 3500
Entry Wire Line
	10150 3400 10250 3500
Entry Wire Line
	10050 3400 10150 3500
Entry Wire Line
	9950 3400 10050 3500
$Comp
L Kur-Princ:ATmega8515 DD1
U 1 1 621D89C6
P 6500 4600
F 0 "DD1" H 6950 6550 50  0000 C CNN
F 1 "ATmega8515-16PU" H 6000 6550 50  0000 C CNN
F 2 "Package_DIP:DIP-40_W15.24mm" H 9700 1000 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/doc2512.pdf" H 6500 5100 50  0001 C CNN
	1    6500 4600
	1    0    0    -1  
$EndComp
$Comp
L Kur-Princ:Program XP2
U 1 1 61ADA9CD
P 9500 1300
F 0 "XP2" H 9600 2000 50  0000 L CNN
F 1 "Разъем программатора AVR-ISP-6" H 9050 1900 50  0000 L CNN
F 2 "Package_DIP:DIP-6_W7.62mm" H 9700 1900 50  0001 C CNN
F 3 "" H 9700 1900 50  0001 C CNN
	1    9500 1300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 1450 8750 1600
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0102
U 1 1 61ADEE6D
P 8750 1600
F 0 "#PWR0102" H 8750 1350 50  0001 C CNN
F 1 "GND" H 8755 1427 50  0000 C CNN
F 2 "" H 8750 1600 50  0001 C CNN
F 3 "" H 8750 1600 50  0001 C CNN
	1    8750 1600
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8750 900  8750 1050
$Comp
L Principle-rescue:+5V-power-Principle-rescue #PWR0104
U 1 1 61AE5F88
P 8750 900
F 0 "#PWR0104" H 8750 750 50  0001 C CNN
F 1 "+5V" H 8765 1073 50  0000 C CNN
F 2 "" H 8750 900 50  0001 C CNN
F 3 "" H 8750 900 50  0001 C CNN
	1    8750 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8750 1050 8850 1050
Wire Wire Line
	8750 1450 8850 1450
Text Label 8100 950  0    50   ~ 0
SPI_MISO
Entry Bus Bus
	8000 3400 8100 3500
Entry Wire Line
	8000 850  8100 950 
Entry Wire Line
	8000 1050 8100 1150
Entry Wire Line
	8000 1150 8100 1250
Entry Wire Line
	8000 1250 8100 1350
Wire Wire Line
	8100 950  8850 950 
Wire Wire Line
	8100 1150 8850 1150
Wire Wire Line
	8100 1250 8850 1250
Wire Wire Line
	8100 1350 8850 1350
Entry Bus Bus
	7900 2000 8000 2100
Wire Bus Line
	4000 2000 7900 2000
Wire Wire Line
	7100 2900 7900 2900
Wire Wire Line
	7100 3000 7900 3000
Wire Wire Line
	7100 3100 7900 3100
Text Label 8100 1250 0    50   ~ 0
SPI_MOSI
Text Label 8100 1150 0    50   ~ 0
SPI_SCK
Text Label 8100 1350 0    50   ~ 0
RESET
Wire Wire Line
	5750 3300 5900 3300
Wire Wire Line
	7100 3200 7900 3200
Text Label 7550 4300 0    50   ~ 0
SPI_MOSI
Text Label 7750 4000 0    50   ~ 0
E
Text Label 7750 3900 0    50   ~ 0
RW
Text Label 7750 3800 0    50   ~ 0
RS
Entry Wire Line
	7900 4000 8000 4100
Entry Wire Line
	7900 3900 8000 4000
Entry Wire Line
	7900 3800 8000 3900
Wire Wire Line
	7100 3800 7900 3800
Wire Wire Line
	7100 3900 7900 3900
Wire Wire Line
	7100 4000 7900 4000
Text Label 7550 4500 0    50   ~ 0
SPI_SCK
Text Label 7550 4400 0    50   ~ 0
SPI_MISO
Entry Wire Line
	7900 4500 8000 4600
Entry Wire Line
	7900 4400 8000 4500
Entry Wire Line
	7900 4300 8000 4400
Entry Wire Line
	7900 4200 8000 4300
Wire Wire Line
	7100 4200 7900 4200
Wire Wire Line
	7100 4300 7900 4300
Wire Wire Line
	7100 4400 7900 4400
Wire Wire Line
	7100 4500 7900 4500
Text Label 7550 4200 0    50   ~ 0
SPI_SS
NoConn ~ 7100 4100
Entry Wire Line
	10750 3400 10850 3500
Text Label 10650 3400 1    50   ~ 0
SPI_SS
Text Label 10850 3400 1    50   ~ 0
SPI_MISO
Text Label 10950 3400 1    50   ~ 0
SPI_SCK
Text Label 10750 3400 1    50   ~ 0
SPI_MOSI
Wire Wire Line
	10950 2850 10950 3400
Wire Wire Line
	10850 2850 10850 3400
Wire Wire Line
	10750 2850 10750 3400
Wire Wire Line
	10650 2850 10650 3400
Wire Wire Line
	9950 2850 9950 3400
Wire Wire Line
	10050 2850 10050 3400
Wire Wire Line
	10150 2850 10150 3400
$Comp
L Principle-rescue:C-Device-Principle-rescue C7
U 1 1 618D7789
P 8500 4550
F 0 "C7" H 8250 4600 50  0000 L CNN
F 1 "1мк" H 8200 4500 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 8538 4400 50  0001 C CNN
F 3 "~" H 8500 4550 50  0001 C CNN
	1    8500 4550
	-1   0    0    -1  
$EndComp
Wire Wire Line
	8800 4400 8500 4400
Wire Wire Line
	8800 4700 8500 4700
$Comp
L Principle-rescue:C-Device-Principle-rescue C4
U 1 1 618E537D
P 10500 4550
F 0 "C4" H 10650 4650 50  0000 L CNN
F 1 "1мк" H 10600 4550 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 10538 4400 50  0001 C CNN
F 3 "~" H 10500 4550 50  0001 C CNN
	1    10500 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10400 4700 10500 4700
NoConn ~ 10400 5400
NoConn ~ 10400 5800
NoConn ~ 11350 5650
NoConn ~ 11350 6450
$Comp
L Principle-rescue:C-Device-Principle-rescue C5
U 1 1 6187048E
P 10850 4900
F 0 "C5" V 11100 4850 50  0000 L CNN
F 1 "1мк" V 11000 4800 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 10888 4750 50  0001 C CNN
F 3 "~" H 10850 4900 50  0001 C CNN
	1    10850 4900
	0    -1   -1   0   
$EndComp
$Comp
L Principle-rescue:C-Device-Principle-rescue C6
U 1 1 61879C5A
P 10650 5200
F 0 "C6" V 10900 5150 50  0000 L CNN
F 1 "1мк" V 10800 5100 50  0000 L CNN
F 2 "Capacitor_SMD:CP_Elec_4x3" H 10688 5050 50  0001 C CNN
F 3 "~" H 10650 5200 50  0001 C CNN
	1    10650 5200
	0    -1   -1   0   
$EndComp
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0101
U 1 1 6187CC33
P 9600 6650
F 0 "#PWR0101" H 9600 6400 50  0001 C CNN
F 1 "GND" H 9605 6477 50  0000 C CNN
F 2 "" H 9600 6650 50  0001 C CNN
F 3 "" H 9600 6650 50  0001 C CNN
	1    9600 6650
	-1   0    0    -1  
$EndComp
NoConn ~ 8800 5800
NoConn ~ 8800 5400
$Comp
L Principle-rescue:+5V-power-Principle-rescue #PWR0110
U 1 1 61980580
P 11150 4750
F 0 "#PWR0110" H 11150 4600 50  0001 C CNN
F 1 "+5V" H 11165 4923 50  0000 C CNN
F 2 "" H 11150 4750 50  0001 C CNN
F 3 "" H 11150 4750 50  0001 C CNN
	1    11150 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 6500 9600 6650
Wire Wire Line
	10500 4400 10400 4400
Wire Wire Line
	10400 5200 10500 5200
Wire Wire Line
	10800 5200 11150 5200
Wire Wire Line
	10700 4900 10400 4900
Wire Wire Line
	11150 4750 11150 4900
Wire Wire Line
	11150 4900 11000 4900
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0115
U 1 1 61D4BEBE
P 11150 6150
F 0 "#PWR0115" H 11150 5900 50  0001 C CNN
F 1 "GND" H 11155 5977 50  0000 C CNN
F 2 "" H 11150 6150 50  0001 C CNN
F 3 "" H 11150 6150 50  0001 C CNN
	1    11150 6150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	11350 6050 11150 6050
Wire Wire Line
	11150 6050 11150 6150
$Comp
L Principle-rescue:DB9-Kur-Princ XP3
U 1 1 61BCDEC2
P 12000 6000
F 0 "XP3" H 12050 6650 50  0000 L CNN
F 1 "Разъем RS-232 DB9" H 11700 6550 50  0000 L CNN
F 2 "Connector_Dsub:DSUB-9_Female_EdgeMount_P2.77mm" H 12000 6000 50  0001 C CNN
F 3 " ~" H 12000 6000 50  0001 C CNN
	1    12000 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 5600 8500 5600
Wire Wire Line
	7100 5700 8500 5700
Wire Wire Line
	10900 5600 10900 5850
Wire Wire Line
	11350 5750 11000 5750
Wire Wire Line
	11000 5750 11000 6000
Wire Wire Line
	10400 6000 11000 6000
Wire Wire Line
	10900 5850 11350 5850
Wire Wire Line
	10400 5600 10900 5600
$Comp
L Principle-rescue:+5V-power-Principle-rescue #PWR0103
U 1 1 621F421D
P 9600 3950
F 0 "#PWR0103" H 9600 3800 50  0001 C CNN
F 1 "+5V" H 9615 4123 50  0000 C CNN
F 2 "" H 9600 3950 50  0001 C CNN
F 3 "" H 9600 3950 50  0001 C CNN
	1    9600 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9600 3950 9600 4000
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0111
U 1 1 6221FB89
P 11150 5350
F 0 "#PWR0111" H 11150 5100 50  0001 C CNN
F 1 "GND" H 11155 5177 50  0000 C CNN
F 2 "" H 11150 5350 50  0001 C CNN
F 3 "" H 11150 5350 50  0001 C CNN
	1    11150 5350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	11150 5200 11150 5350
Wire Wire Line
	5500 3100 5900 3100
Wire Wire Line
	4100 2900 5900 2900
Entry Wire Line
	7650 6000 7750 6100
Entry Wire Line
	7650 6300 7750 6400
Wire Wire Line
	7650 6000 7100 6000
Wire Wire Line
	7100 6300 7650 6300
$Comp
L Kur-Princ:MAX232 DD3
U 1 1 621CBB95
P 9600 5300
F 0 "DD3" H 10100 6350 50  0000 C CNN
F 1 "MAX232" H 9150 6350 50  0000 C CNN
F 2 "Package_SO:SOIC-16W_7.5x10.3mm_P1.27mm" H 9650 4100 50  0001 L CNN
F 3 "http://www.ti.com/lit/ds/symlink/max232.pdf" H 9600 5400 50  0001 C CNN
	1    9600 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 5700 8500 5600
Wire Wire Line
	8400 5600 8400 6000
Wire Wire Line
	8400 6000 8800 6000
Wire Wire Line
	7100 5600 8400 5600
Wire Wire Line
	4900 3700 5050 3700
Wire Wire Line
	5050 3650 5050 3700
$Comp
L Principle-rescue:XTAL-Kur-Princ ZQ1
U 1 1 61B5B578
P 5050 3500
F 0 "ZQ1" V 5100 3250 50  0000 C CNN
F 1 "8МГц" V 5000 3250 50  0000 C CNN
F 2 "Crystal:Crystal_AT310_D3.0mm_L10.0mm_Horizontal" H 5050 3500 50  0001 C CNN
F 3 "~" H 5050 3500 50  0001 C CNN
	1    5050 3500
	0    -1   -1   0   
$EndComp
Connection ~ 5050 3300
Connection ~ 5050 3700
Wire Wire Line
	5050 3300 5500 3300
Wire Wire Line
	5050 3700 5500 3700
$Comp
L Principle-rescue:+5V-power-Principle-rescue #PWR0105
U 1 1 61AE0696
P 3500 1950
F 0 "#PWR0105" H 3500 1800 50  0001 C CNN
F 1 "+5V" H 3515 2123 50  0000 C CNN
F 2 "" H 3500 1950 50  0001 C CNN
F 3 "" H 3500 1950 50  0001 C CNN
	1    3500 1950
	1    0    0    -1  
$EndComp
$Comp
L Principle-rescue:R-Device-Principle-rescue R1
U 1 1 61AE47B3
P 3500 2250
F 0 "R1" H 3600 2300 50  0000 L CNN
F 1 "10к" H 3570 2205 50  0000 L CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3430 2250 50  0001 C CNN
F 3 "~" H 3500 2250 50  0001 C CNN
	1    3500 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4000 2500 4500
Connection ~ 2500 4500
Wire Wire Line
	2500 4500 2500 4650
Wire Wire Line
	3500 2500 3500 2400
Connection ~ 3500 2500
Wire Wire Line
	3500 2500 3900 2500
Wire Wire Line
	3500 1950 3500 2100
$Comp
L Principle-rescue:C-Device-Principle-rescue C3
U 1 1 61B29DC8
P 6500 8550
F 0 "C3" H 6600 8600 50  0000 L CNN
F 1 "100н" H 6615 8505 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6538 8400 50  0001 C CNN
F 3 "~" H 6500 8550 50  0001 C CNN
	1    6500 8550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6500 8200 6500 8350
Wire Wire Line
	6000 8350 6500 8350
Connection ~ 6000 8350
Connection ~ 6500 8350
Wire Wire Line
	6500 8350 6500 8400
Wire Wire Line
	6500 8700 6500 8750
Wire Wire Line
	6500 8750 6000 8750
Connection ~ 6000 8750
$Comp
L Principle-rescue:C-Device-Principle-rescue C10
U 1 1 61B15044
P 6800 2450
F 0 "C10" V 7050 2400 50  0000 L CNN
F 1 "100н" V 6950 2350 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6838 2300 50  0001 C CNN
F 3 "~" H 6800 2450 50  0001 C CNN
	1    6800 2450
	0    -1   -1   0   
$EndComp
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0117
U 1 1 61B30431
P 7250 2500
F 0 "#PWR0117" H 7250 2250 50  0001 C CNN
F 1 "GND" H 7255 2327 50  0000 C CNN
F 2 "" H 7250 2500 50  0001 C CNN
F 3 "" H 7250 2500 50  0001 C CNN
	1    7250 2500
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6650 2450 6500 2450
Wire Wire Line
	6500 2450 6500 2300
Connection ~ 6500 2450
Wire Wire Line
	7250 2450 7250 2500
Wire Wire Line
	6950 2450 7250 2450
$Comp
L Principle-rescue:C-Device-Principle-rescue C11
U 1 1 61B668AA
P 9900 4000
F 0 "C11" V 10150 3950 50  0000 L CNN
F 1 "100н" V 10050 3900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9938 3850 50  0001 C CNN
F 3 "~" H 9900 4000 50  0001 C CNN
	1    9900 4000
	0    -1   -1   0   
$EndComp
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0118
U 1 1 61B668B0
P 10350 4050
F 0 "#PWR0118" H 10350 3800 50  0001 C CNN
F 1 "GND" H 10355 3877 50  0000 C CNN
F 2 "" H 10350 4050 50  0001 C CNN
F 3 "" H 10350 4050 50  0001 C CNN
	1    10350 4050
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10350 4000 10350 4050
Wire Wire Line
	10050 4000 10350 4000
Wire Wire Line
	9600 4000 9750 4000
Connection ~ 9600 4000
Wire Wire Line
	9600 4000 9600 4100
$Comp
L Principle-rescue:C-Device-Principle-rescue C12
U 1 1 61B91C82
P 9050 3000
F 0 "C12" V 9300 2950 50  0000 L CNN
F 1 "100н" V 9200 2900 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 9088 2850 50  0001 C CNN
F 3 "~" H 9050 3000 50  0001 C CNN
	1    9050 3000
	0    1    -1   0   
$EndComp
$Comp
L Principle-rescue:GND-power-Principle-rescue #PWR0119
U 1 1 61B91C88
P 8600 3050
F 0 "#PWR0119" H 8600 2800 50  0001 C CNN
F 1 "GND" H 8605 2877 50  0000 C CNN
F 2 "" H 8600 3050 50  0001 C CNN
F 3 "" H 8600 3050 50  0001 C CNN
	1    8600 3050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8600 3000 8600 3050
Wire Wire Line
	8900 3000 8600 3000
Wire Wire Line
	9350 2850 9350 2900
Wire Wire Line
	9350 2900 9700 2900
Wire Wire Line
	9350 2900 9350 3000
Wire Wire Line
	9350 3000 9200 3000
Connection ~ 9350 2900
NoConn ~ 11350 6350
NoConn ~ 11350 6250
NoConn ~ 11350 6150
NoConn ~ 11350 5950
Wire Bus Line
	4000 2000 4000 4750
Wire Bus Line
	8100 3500 11250 3500
Wire Bus Line
	7750 5900 7750 8000
Wire Bus Line
	8000 750  8000 4750
$EndSCHEMATC
