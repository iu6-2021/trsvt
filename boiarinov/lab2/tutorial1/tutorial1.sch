EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Урок 1"
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 616A2D84
P 10050 3100
F 0 "R1" V 9843 3100 50  0000 C CNN
F 1 "1k" V 9934 3100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 9980 3100 50  0001 C CNN
F 3 "~" H 10050 3100 50  0001 C CNN
	1    10050 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 616A382E
P 10400 4950
F 0 "R2" V 10193 4950 50  0000 C CNN
F 1 "100" V 10284 4950 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 10330 4950 50  0001 C CNN
F 3 "~" H 10400 4950 50  0001 C CNN
	1    10400 4950
	0    1    1    0   
$EndComp
$Comp
L Device:LED D1
U 1 1 616A4FA9
P 9450 3100
F 0 "D1" H 9443 3317 50  0000 C CNN
F 1 "LED" H 9443 3226 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 9450 3100 50  0001 C CNN
F 3 "~" H 9450 3100 50  0001 C CNN
	1    9450 3100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR06
U 1 1 6170ABD0
P 10400 3000
F 0 "#PWR06" H 10400 2850 50  0001 C CNN
F 1 "VCC" H 10415 3173 50  0000 C CNN
F 2 "" H 10400 3000 50  0001 C CNN
F 3 "" H 10400 3000 50  0001 C CNN
	1    10400 3000
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 6170B43A
P 8050 2400
F 0 "#PWR01" H 8050 2250 50  0001 C CNN
F 1 "VCC" H 8065 2573 50  0000 C CNN
F 2 "" H 8050 2400 50  0001 C CNN
F 3 "" H 8050 2400 50  0001 C CNN
	1    8050 2400
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 6170BD66
P 9800 4500
F 0 "#PWR03" H 9800 4350 50  0001 C CNN
F 1 "VCC" H 9815 4673 50  0000 C CNN
F 2 "" H 9800 4500 50  0001 C CNN
F 3 "" H 9800 4500 50  0001 C CNN
	1    9800 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 6170C656
P 9750 5200
F 0 "#PWR04" H 9750 4950 50  0001 C CNN
F 1 "GND" H 9755 5027 50  0000 C CNN
F 2 "" H 9750 5200 50  0001 C CNN
F 3 "" H 9750 5200 50  0001 C CNN
	1    9750 5200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR02
U 1 1 6170CD27
P 8050 5050
F 0 "#PWR02" H 8050 4800 50  0001 C CNN
F 1 "GND" H 8055 4877 50  0000 C CNN
F 2 "" H 8050 5050 50  0001 C CNN
F 3 "" H 8050 5050 50  0001 C CNN
	1    8050 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	10200 3100 10400 3100
Wire Wire Line
	10400 3100 10400 3000
Wire Wire Line
	9150 3100 9300 3100
Wire Wire Line
	9600 3100 9900 3100
Text Label 9150 3500 0    50   ~ 0
INPUT
Text Label 10850 4950 0    50   ~ 0
INPUT
Text Label 9050 2900 0    50   ~ 0
uCtoLED
Text Label 9650 3100 0    50   ~ 0
LEDtoR
Text Label 9900 4950 0    50   ~ 0
INPUTtoR
$Comp
L power:PWR_FLAG #FLG01
U 1 1 617111C9
P 10000 3550
F 0 "#FLG01" H 10000 3625 50  0001 C CNN
F 1 "PWR_FLAG" H 10000 3723 50  0000 C CNN
F 2 "" H 10000 3550 50  0001 C CNN
F 3 "~" H 10000 3550 50  0001 C CNN
	1    10000 3550
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR05
U 1 1 617132A8
P 10000 3850
F 0 "#PWR05" H 10000 3600 50  0001 C CNN
F 1 "GND" H 10005 3677 50  0000 C CNN
F 2 "" H 10000 3850 50  0001 C CNN
F 3 "" H 10000 3850 50  0001 C CNN
	1    10000 3850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 6171368C
P 10500 3850
F 0 "#PWR07" H 10500 3700 50  0001 C CNN
F 1 "VCC" H 10515 4023 50  0000 C CNN
F 2 "" H 10500 3850 50  0001 C CNN
F 3 "" H 10500 3850 50  0001 C CNN
	1    10500 3850
	-1   0    0    1   
$EndComp
Text Notes 10600 3650 0    50   ~ 0
<-- Некоторая очень полезная приблуда
Wire Wire Line
	10550 4950 11200 4950
$Comp
L MCU_Microchip_PIC18:PIC18F23K22-xSP U1
U 1 1 6181902A
P 8050 3700
F 0 "U1" H 8050 4881 50  0000 C CNN
F 1 "PIC18F23K22-xSP" H 8050 4790 50  0000 C CNN
F 2 "Package_DIP:DIP-28_W7.62mm" H 8100 3725 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001412G.pdf" H 8100 3725 50  0001 C CNN
	1    8050 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	8050 2700 8050 2400
Wire Wire Line
	8050 4700 8050 5050
Wire Wire Line
	8750 2900 9150 2900
Wire Wire Line
	9150 2900 9150 3100
Wire Wire Line
	8750 3000 8950 3000
Wire Wire Line
	8950 3000 8950 3500
Wire Wire Line
	8950 3500 9450 3500
NoConn ~ 7350 3100
NoConn ~ 7350 3500
NoConn ~ 7350 3600
NoConn ~ 7350 3700
NoConn ~ 7350 3800
NoConn ~ 7350 3900
NoConn ~ 7350 4000
NoConn ~ 7350 4100
NoConn ~ 7350 4200
NoConn ~ 8750 3100
NoConn ~ 8750 3200
NoConn ~ 8750 3300
NoConn ~ 8750 3400
NoConn ~ 8750 3500
NoConn ~ 8750 3600
NoConn ~ 8750 3800
NoConn ~ 8750 3900
NoConn ~ 8750 4000
NoConn ~ 8750 4100
NoConn ~ 8750 4200
NoConn ~ 8750 4300
NoConn ~ 8750 4400
NoConn ~ 8750 4500
$Comp
L power:PWR_FLAG #FLG02
U 1 1 61712DE8
P 10500 3550
F 0 "#FLG02" H 10500 3625 50  0001 C CNN
F 1 "PWR_FLAG" H 10500 3723 50  0000 C CNN
F 2 "" H 10500 3550 50  0001 C CNN
F 3 "~" H 10500 3550 50  0001 C CNN
	1    10500 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	10000 3550 10000 3850
Wire Wire Line
	10500 3550 10500 3850
Wire Wire Line
	9500 4850 9800 4850
Wire Wire Line
	9800 4850 9800 4500
Wire Wire Line
	9500 5050 9750 5050
Wire Wire Line
	9750 5050 9750 5200
Wire Wire Line
	9500 4950 10250 4950
$Comp
L myLib:GostConnector X11
U 1 1 6181E668
P 9300 4750
F 0 "X11" H 9208 4965 50  0000 C CNN
F 1 "GostConnector" H 9208 4874 50  0000 C CNN
F 2 "Connector:Banana_Jack_3Pin" H 9300 4750 50  0001 C CNN
F 3 "" H 9300 4750 50  0001 C CNN
	1    9300 4750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
