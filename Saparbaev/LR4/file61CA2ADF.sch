EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	5000 2025 5400 2025
Text HLabel 5400 2025 2    50   Output ~ 0
5V
Text HLabel 3800 2025 0    50   Input ~ 0
VIN
Wire Wire Line
	4100 2675 4500 2675
Wire Wire Line
	4100 2575 4100 2675
Wire Wire Line
	4500 2675 4500 2825
Connection ~ 4500 2675
Wire Wire Line
	5000 2675 4500 2675
Wire Wire Line
	5000 2575 5000 2675
$Comp
L power:Earth #PWR?
U 1 1 61CAD3FF
P 4500 2825
F 0 "#PWR?" H 4500 2575 50  0001 C CNN
F 1 "Earth" H 4500 2675 50  0001 C CNN
F 2 "" H 4500 2825 50  0001 C CNN
F 3 "~" H 4500 2825 50  0001 C CNN
	1    4500 2825
	1    0    0    -1  
$EndComp
Wire Wire Line
	4100 2025 3800 2025
Connection ~ 4100 2025
Wire Wire Line
	4100 2275 4100 2025
Connection ~ 5000 2025
Wire Wire Line
	5000 2275 5000 2025
Wire Wire Line
	4800 2025 5000 2025
Wire Wire Line
	4200 2025 4100 2025
$Comp
L Device:C C?
U 1 1 61CA8A7A
P 5000 2425
F 0 "C?" H 5115 2471 50  0000 L CNN
F 1 "10uF" H 5115 2380 50  0000 L CNN
F 2 "" H 5038 2275 50  0001 C CNN
F 3 "~" H 5000 2425 50  0001 C CNN
	1    5000 2425
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 61CA7132
P 4100 2425
F 0 "C?" H 4215 2471 50  0000 L CNN
F 1 "10uF" H 4215 2380 50  0000 L CNN
F 2 "" H 4138 2275 50  0001 C CNN
F 3 "~" H 4100 2425 50  0001 C CNN
	1    4100 2425
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 2325 4500 2675
$Comp
L Regulator_Linear:LD1117S50TR_SOT223 U?
U 1 1 61CA33EE
P 4500 2025
F 0 "U?" H 4500 2267 50  0000 C CNN
F 1 "LD1117S50TR_SOT223" H 4500 2176 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 4500 2225 50  0001 C CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00000544.pdf" H 4600 1775 50  0001 C CNN
	1    4500 2025
	1    0    0    -1  
$EndComp
$EndSCHEMATC
