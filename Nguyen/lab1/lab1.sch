EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Простая принципиальная схема"
Date ""
Rev ""
Comp "МГТУ им. Н.Э.Баумана\\nГруппа ИУ6И-71Б"
Comment1 "Лабораторная работа 1"
Comment2 "Нгуен Ань Ту"
Comment3 "Хохлов С. А."
Comment4 ""
$EndDescr
Wire Wire Line
	6900 6100 6900 6250
$Comp
L power:GND #PWR01
U 1 1 616A9F0F
P 6900 6250
F 0 "#PWR01" H 6900 6000 50  0001 C CNN
F 1 "GND" H 6905 6077 50  0000 C CNN
F 2 "" H 6900 6250 50  0001 C CNN
F 3 "" H 6900 6250 50  0001 C CNN
	1    6900 6250
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG01
U 1 1 616A69D1
P 6900 6100
F 0 "#FLG01" H 6900 6175 50  0001 C CNN
F 1 "PWR_FLAG" H 6900 6273 50  0000 C CNN
F 2 "" H 6900 6100 50  0001 C CNN
F 3 "~" H 6900 6100 50  0001 C CNN
	1    6900 6100
	1    0    0    -1  
$EndComp
NoConn ~ 8550 5450
NoConn ~ 7350 5450
NoConn ~ 7350 5350
NoConn ~ 7350 5250
Text Label 9050 6050 0    50   ~ 0
INPUTtoR
Text Label 9500 5350 1    50   ~ 0
LEDtoR
Text Label 8750 5200 0    50   ~ 0
uCtoLED
Text Label 8950 5350 0    50   ~ 0
INPUT
Text Label 9950 6050 0    50   ~ 0
INPUT
Wire Wire Line
	9700 6050 10250 6050
Wire Wire Line
	8900 6050 9400 6050
Wire Wire Line
	8550 5350 9250 5350
Wire Wire Line
	8750 5250 8550 5250
Wire Wire Line
	8750 5050 8750 5250
Wire Wire Line
	9100 5050 8750 5050
Wire Wire Line
	7950 4650 7950 4750
Wire Wire Line
	7950 6050 7950 5950
Wire Wire Line
	9000 6150 8900 6150
Wire Wire Line
	9000 6250 9000 6150
Wire Wire Line
	9000 5950 8900 5950
Wire Wire Line
	9000 5800 9000 5950
Wire Wire Line
	9600 5050 9400 5050
Wire Wire Line
	10050 5050 9900 5050
Wire Wire Line
	10050 4850 10050 5050
$Comp
L power:GND #PWR06
U 1 1 6169F398
P 9000 6250
F 0 "#PWR06" H 9000 6000 50  0001 C CNN
F 1 "GND" H 9005 6077 50  0000 C CNN
F 2 "" H 9000 6250 50  0001 C CNN
F 3 "" H 9000 6250 50  0001 C CNN
	1    9000 6250
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 6169EC9F
P 7950 6050
F 0 "#PWR04" H 7950 5800 50  0001 C CNN
F 1 "GND" H 7955 5877 50  0000 C CNN
F 2 "" H 7950 6050 50  0001 C CNN
F 3 "" H 7950 6050 50  0001 C CNN
	1    7950 6050
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 6169E6E1
P 9000 5800
F 0 "#PWR05" H 9000 5650 50  0001 C CNN
F 1 "VCC" H 9015 5973 50  0000 C CNN
F 2 "" H 9000 5800 50  0001 C CNN
F 3 "" H 9000 5800 50  0001 C CNN
	1    9000 5800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 6169CF6E
P 10050 4850
F 0 "#PWR07" H 10050 4700 50  0001 C CNN
F 1 "VCC" H 10065 5023 50  0000 C CNN
F 2 "" H 10050 4850 50  0001 C CNN
F 3 "" H 10050 4850 50  0001 C CNN
	1    10050 4850
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR03
U 1 1 6169A55D
P 7950 4650
F 0 "#PWR03" H 7950 4500 50  0001 C CNN
F 1 "VCC" H 7965 4823 50  0000 C CNN
F 2 "" H 7950 4650 50  0001 C CNN
F 3 "" H 7950 4650 50  0001 C CNN
	1    7950 4650
	1    0    0    -1  
$EndComp
$Comp
L myLib:MYCONN3 J1
U 1 1 61699F9C
P 8600 5850
F 0 "J1" H 8517 5975 50  0000 C CNN
F 1 "MYCONN3" H 8517 5884 50  0000 C CNN
F 2 "" H 8600 5450 50  0001 C CNN
F 3 "" H 8600 5450 50  0001 C CNN
	1    8600 5850
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 61697C57
P 9250 5050
F 0 "D1" H 9243 5267 50  0000 C CNN
F 1 "LED" H 9243 5176 50  0000 C CNN
F 2 "" H 9250 5050 50  0001 C CNN
F 3 "~" H 9250 5050 50  0001 C CNN
	1    9250 5050
	1    0    0    -1  
$EndComp
$Comp
L microchip_pic12mcu:PIC12C508A-ISN U1
U 1 1 616975D9
P 7950 5350
F 0 "U1" H 8250 5850 50  0000 C CNN
F 1 "PIC12C508A-ISN" H 7550 5850 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 8550 6000 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/40139e.pdf" H 7950 5350 50  0001 C CNN
	1    7950 5350
	-1   0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 61696A41
P 9550 6050
F 0 "R1" V 9757 6050 50  0000 C CNN
F 1 "100" V 9666 6050 50  0000 C CNN
F 2 "" V 9480 6050 50  0001 C CNN
F 3 "~" H 9550 6050 50  0001 C CNN
	1    9550 6050
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R2
U 1 1 616965FF
P 9750 5050
F 0 "R2" V 9543 5050 50  0000 C CNN
F 1 "1k" V 9634 5050 50  0000 C CNN
F 2 "" V 9680 5050 50  0001 C CNN
F 3 "~" H 9750 5050 50  0001 C CNN
	1    9750 5050
	0    1    1    0   
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 616DD495
P 7350 6100
F 0 "#FLG?" H 7350 6175 50  0001 C CNN
F 1 "PWR_FLAG" H 7350 6273 50  0000 C CNN
F 2 "" H 7350 6100 50  0001 C CNN
F 3 "~" H 7350 6100 50  0001 C CNN
	1    7350 6100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR?
U 1 1 616E032C
P 7350 6250
F 0 "#PWR?" H 7350 6100 50  0001 C CNN
F 1 "VCC" H 7365 6423 50  0000 C CNN
F 2 "" H 7350 6250 50  0001 C CNN
F 3 "" H 7350 6250 50  0001 C CNN
	1    7350 6250
	-1   0    0    1   
$EndComp
Wire Wire Line
	7350 6100 7350 6250
$EndSCHEMATC
