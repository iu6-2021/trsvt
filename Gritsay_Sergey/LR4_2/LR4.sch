EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема электрическая принципиальная"
Date ""
Rev ""
Comp "МГТУ им. Н.Э. Баумана"
Comment1 "Курсовая работа по дисциплине \"Микропроцессорные системы\""
Comment2 "Грицай С.В."
Comment3 "Хохлов С.А."
Comment4 ""
$EndDescr
$Comp
L Interface_Expansion:PCF8574 DD5
U 1 1 61B7E8C1
P 13600 6400
F 0 "DD5" H 13300 7100 50  0000 C CNN
F 1 "PCF8574" H 13800 5750 50  0000 C CNN
F 2 "" H 13600 6400 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/PCF8574_PCF8574A.pdf" H 13600 6400 50  0001 C CNN
	1    13600 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	13600 7100 13600 7300
NoConn ~ 13100 6800
$Comp
L power:Earth #PWR010
U 1 1 61BAC0F6
P 12850 5200
F 0 "#PWR010" H 12850 4950 50  0001 C CNN
F 1 "Earth" H 12850 5050 50  0001 C CNN
F 2 "" H 12850 5200 50  0001 C CNN
F 3 "~" H 12850 5200 50  0001 C CNN
	1    12850 5200
	1    0    0    1   
$EndComp
Wire Wire Line
	13600 5700 13600 5400
Wire Wire Line
	13600 5400 12850 5400
Wire Wire Line
	13100 6500 12850 6500
Wire Wire Line
	12850 6500 12850 6400
Wire Wire Line
	13100 6400 12850 6400
Connection ~ 12850 6400
Wire Wire Line
	12850 6400 12850 6300
Wire Wire Line
	13100 6300 12850 6300
Connection ~ 12850 6300
Wire Wire Line
	12850 6300 12850 5400
$Comp
L Display_Character:MT-16S2H-8 DD6
U 1 1 61C1A410
P 15250 7900
F 0 "DD6" H 14350 8450 50  0000 C CNN
F 1 "MT-16S2H-8" H 16000 8450 50  0000 C CNN
F 2 "Display_7Segment:CC56-12GWA" H 15250 7150 50  0001 C CNN
F 3 "http://www.kingbrightusa.com/images/catalog/SPEC/CC56-12GWA.pdf" H 14820 8130 50  0001 C CNN
	1    15250 7900
	1    0    0    -1  
$EndComp
Wire Wire Line
	14100 6000 14150 6000
Wire Wire Line
	14100 6100 14200 6100
Wire Wire Line
	14100 6200 14250 6200
Wire Wire Line
	14100 6300 14300 6300
Wire Wire Line
	14100 6400 14350 6400
Wire Wire Line
	14100 6500 14400 6500
Wire Wire Line
	14100 6600 14450 6600
Wire Wire Line
	14300 6800 14300 6700
Wire Wire Line
	14300 6700 14100 6700
$Comp
L Device:R_Network10 R12
U 1 1 61CF14EA
P 14350 4950
F 0 "R12" H 14838 4996 50  0000 L CNN
F 1 "CTS8917" H 14838 4905 50  0000 L CNN
F 2 "Resistor_THT:R_Array_SIP11" V 14925 4950 50  0001 C CNN
F 3 "http://www.vishay.com/docs/31509/csc.pdf" H 14350 4950 50  0001 C CNN
	1    14350 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	13850 5150 13850 5600
Wire Wire Line
	13850 5600 14150 5600
Wire Wire Line
	14150 5600 14150 6000
Connection ~ 14150 6000
Wire Wire Line
	13950 5150 13950 5550
Wire Wire Line
	13950 5550 14200 5550
Wire Wire Line
	14200 5550 14200 6100
Connection ~ 14200 6100
Wire Wire Line
	14050 5150 14050 5500
Wire Wire Line
	14050 5500 14250 5500
Wire Wire Line
	14250 5500 14250 6200
Connection ~ 14250 6200
Wire Wire Line
	14150 5150 14150 5450
Wire Wire Line
	14150 5450 14300 5450
Wire Wire Line
	14300 5450 14300 6300
Connection ~ 14300 6300
Wire Wire Line
	14250 5150 14250 5400
Wire Wire Line
	14250 5400 14350 5400
Wire Wire Line
	14350 5400 14350 6400
Connection ~ 14350 6400
Wire Wire Line
	14350 5150 14350 5350
Wire Wire Line
	14350 5350 14400 5350
Wire Wire Line
	14400 5350 14400 6500
Connection ~ 14400 6500
Wire Wire Line
	14450 5150 14450 6600
Connection ~ 14450 6600
Wire Wire Line
	14450 6600 14550 6600
Wire Wire Line
	14550 5150 14550 5600
Wire Wire Line
	14550 5600 14500 5600
Wire Wire Line
	14500 5600 14500 6800
Connection ~ 14500 6800
Wire Wire Line
	14500 6800 14300 6800
NoConn ~ 14650 5150
NoConn ~ 14750 5150
$Comp
L power:Earth #PWR011
U 1 1 61D797FE
P 13600 4900
F 0 "#PWR011" H 13600 4650 50  0001 C CNN
F 1 "Earth" H 13600 4750 50  0001 C CNN
F 2 "" H 13600 4900 50  0001 C CNN
F 3 "~" H 13600 4900 50  0001 C CNN
	1    13600 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	12950 7300 13600 7300
Wire Wire Line
	9250 5550 9600 5550
Wire Wire Line
	9250 4200 9600 4200
Wire Wire Line
	9250 5850 9600 5850
Text Label 9600 6850 2    50   ~ 0
VDD3V3
Text Label 9600 6750 2    50   ~ 0
GND
Text Label 9600 6650 2    50   ~ 0
D15
Text Label 9600 6550 2    50   ~ 0
IO2
Text Label 9600 6450 2    50   ~ 0
D4
Text Label 9600 6350 2    50   ~ 0
D16
Text Label 9600 6250 2    50   ~ 0
D17
Text Label 9600 6150 2    50   ~ 0
D5
Text Label 9600 6050 2    50   ~ 0
D18
Text Label 9600 5950 2    50   ~ 0
D19
Text Label 9600 5850 2    50   ~ 0
D21
Text Label 9600 5750 2    50   ~ 0
RX0
Text Label 9600 5650 2    50   ~ 0
TX0
Text Label 9600 5550 2    50   ~ 0
D22
Text Label 9600 5450 2    50   ~ 0
D23
Wire Wire Line
	9600 6850 9250 6850
Wire Wire Line
	9250 6750 9600 6750
Wire Wire Line
	9250 6650 9600 6650
Wire Wire Line
	9250 6550 9600 6550
Wire Wire Line
	9250 6450 9600 6450
Wire Wire Line
	9250 6350 9600 6350
Wire Wire Line
	9600 6250 9250 6250
Wire Wire Line
	9250 6150 9600 6150
Wire Wire Line
	9600 6050 9250 6050
Wire Wire Line
	9250 5950 9600 5950
Wire Wire Line
	9250 5750 9600 5750
Wire Wire Line
	9250 5650 9600 5650
Wire Wire Line
	9250 5450 9600 5450
Text Label 9600 4300 2    50   ~ 0
GND
Text Label 9600 4200 2    50   ~ 0
VIN
Text Label 9600 4100 2    50   ~ 0
D13
Text Label 9600 4000 2    50   ~ 0
D12
Text Label 9600 3900 2    50   ~ 0
D14
Text Label 9600 3800 2    50   ~ 0
D27
Text Label 9600 3700 2    50   ~ 0
D26
Text Label 9600 3600 2    50   ~ 0
D25
Text Label 9600 3500 2    50   ~ 0
D33
Text Label 9600 3400 2    50   ~ 0
D32
Text Label 9600 3300 2    50   ~ 0
D35
Text Label 9600 3200 2    50   ~ 0
D34
Text Label 9600 3100 2    50   ~ 0
VN
Text Label 9600 3000 2    50   ~ 0
VP
Text Label 9600 2900 2    50   ~ 0
EN
Wire Wire Line
	9600 4300 9250 4300
Wire Wire Line
	9250 4100 9600 4100
Wire Wire Line
	9250 4000 9600 4000
Wire Wire Line
	9250 3900 9600 3900
Wire Wire Line
	9250 3800 9600 3800
Wire Wire Line
	9600 3700 9250 3700
Wire Wire Line
	9250 3600 9600 3600
Wire Wire Line
	9600 3500 9250 3500
Wire Wire Line
	9250 3400 9600 3400
Wire Wire Line
	9250 3300 9600 3300
Wire Wire Line
	9250 3200 9600 3200
Wire Wire Line
	9250 3100 9600 3100
Wire Wire Line
	9250 3000 9600 3000
Wire Wire Line
	9250 2900 9600 2900
Wire Wire Line
	3000 2500 3750 2500
Wire Wire Line
	3000 2250 3000 2500
Wire Wire Line
	2550 2250 3000 2250
Wire Wire Line
	2550 2350 3550 2350
Wire Wire Line
	2550 2450 3450 2450
Connection ~ 2650 2650
Wire Wire Line
	2650 2550 2650 2650
Wire Wire Line
	2550 2550 2650 2550
Wire Wire Line
	2650 2650 2650 2800
Wire Wire Line
	2550 2650 2650 2650
$Comp
L power:Earth #PWR01
U 1 1 61C1F0C1
P 2650 2800
F 0 "#PWR01" H 2650 2550 50  0001 C CNN
F 1 "Earth" H 2650 2650 50  0001 C CNN
F 2 "" H 2650 2800 50  0001 C CNN
F 3 "~" H 2650 2800 50  0001 C CNN
	1    2650 2800
	1    0    0    -1  
$EndComp
$Comp
L My_Conn:XP1 XP1
U 1 1 61C05E40
P 2050 2100
F 0 "XP1" H 2050 2150 50  0000 C CNN
F 1 "XP1" H 2050 2150 50  0001 C CNN
F 2 "" H 2050 2150 50  0001 C CNN
F 3 "" H 2050 2150 50  0001 C CNN
	1    2050 2100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1550 3900 1450 3900
Wire Wire Line
	1850 4200 1850 4450
$Comp
L Device:C C1
U 1 1 61B5B63D
P 1150 4150
F 0 "C1" H 1000 4250 50  0000 L CNN
F 1 "100nF" H 950 4050 50  0000 L CNN
F 2 "" H 1188 4000 50  0001 C CNN
F 3 "~" H 1150 4150 50  0001 C CNN
	1    1150 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 61B5D42F
P 2750 4200
F 0 "C3" H 2865 4246 50  0000 L CNN
F 1 "100nF" H 2865 4155 50  0000 L CNN
F 2 "" H 2788 4050 50  0001 C CNN
F 3 "~" H 2750 4200 50  0001 C CNN
	1    2750 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 4350 2500 4500
Wire Wire Line
	2750 4050 2750 3900
Connection ~ 2750 3900
Wire Wire Line
	2750 3900 2900 3900
Wire Wire Line
	2750 4550 2750 4500
Wire Wire Line
	1150 4300 1150 4450
Wire Wire Line
	1150 4000 1150 3900
Connection ~ 1150 3900
Text Label 2900 3900 0    50   ~ 0
VDD3V3
$Comp
L power:Earth #PWR0103
U 1 1 61B63C02
P 1850 4500
F 0 "#PWR0103" H 1850 4250 50  0001 C CNN
F 1 "Earth" H 1850 4350 50  0001 C CNN
F 2 "" H 1850 4500 50  0001 C CNN
F 3 "~" H 1850 4500 50  0001 C CNN
	1    1850 4500
	1    0    0    -1  
$EndComp
$Comp
L power:Earth #PWR0104
U 1 1 61B643F6
P 2750 4550
F 0 "#PWR0104" H 2750 4300 50  0001 C CNN
F 1 "Earth" H 2750 4400 50  0001 C CNN
F 2 "" H 2750 4550 50  0001 C CNN
F 3 "~" H 2750 4550 50  0001 C CNN
	1    2750 4550
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:AMS1117-3.3 DD1
U 1 1 61B55B19
P 1850 3900
F 0 "DD1" H 1850 4142 50  0000 C CNN
F 1 "AMS1117-3.3" H 1850 4051 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-223-3_TabPin2" H 1850 4100 50  0001 C CNN
F 3 "http://www.advanced-monolithic.com/pdf/ds1117.pdf" H 1950 3650 50  0001 C CNN
	1    1850 3900
	1    0    0    -1  
$EndComp
Wire Wire Line
	2500 3900 2500 4050
Connection ~ 2500 3900
Wire Wire Line
	2500 3900 2750 3900
Wire Wire Line
	2500 4500 2750 4500
Connection ~ 2750 4500
Wire Wire Line
	2750 4500 2750 4350
Wire Wire Line
	1150 4450 1450 4450
Connection ~ 1850 4450
Wire Wire Line
	1850 4450 1850 4500
Wire Wire Line
	2150 3900 2500 3900
Wire Wire Line
	1150 3550 1150 3900
Text Label 1150 3550 0    50   ~ 0
VIN
Wire Wire Line
	1450 4000 1450 3900
Connection ~ 1450 3900
Wire Wire Line
	1450 3900 1150 3900
Wire Wire Line
	1450 4300 1450 4450
Connection ~ 1450 4450
Wire Wire Line
	1450 4450 1850 4450
$Comp
L Device:CP C2
U 1 1 61BB0231
P 1450 4150
F 0 "C2" H 1500 4250 50  0000 L CNN
F 1 "10uF" H 1500 4050 50  0000 L CNN
F 2 "" H 1488 4000 50  0001 C CNN
F 3 "~" H 1450 4150 50  0001 C CNN
	1    1450 4150
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C6
U 1 1 61BB0DF4
P 2500 4200
F 0 "C6" H 2550 4350 50  0000 L CNN
F 1 "10uF" H 2500 4100 50  0000 L CNN
F 2 "" H 2538 4050 50  0001 C CNN
F 3 "~" H 2500 4200 50  0001 C CNN
	1    2500 4200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2700 3750 2700
Wire Wire Line
	3450 2450 3450 2700
Text Label 2800 2250 0    50   ~ 0
VIN
NoConn ~ 5250 7300
Text Label 7800 4400 0    50   ~ 0
IO0
Text Label 7800 2700 0    50   ~ 0
EN
Text Label 5850 3150 0    50   ~ 0
DTR
Text Label 5850 4050 0    50   ~ 0
RTS
Wire Wire Line
	6250 3150 5850 3150
Connection ~ 6250 3150
Wire Wire Line
	6250 3850 6250 3150
Wire Wire Line
	7400 3850 6250 3850
Wire Wire Line
	6200 4050 5850 4050
Connection ~ 6200 4050
Wire Wire Line
	6200 3350 6200 4050
Wire Wire Line
	7400 3350 6200 3350
Wire Wire Line
	6450 3150 6250 3150
Wire Wire Line
	6450 4050 6200 4050
Wire Wire Line
	7400 2700 7800 2700
Wire Wire Line
	7400 2950 7400 2700
Wire Wire Line
	7400 4400 7800 4400
Wire Wire Line
	7400 4250 7400 4400
Wire Wire Line
	7100 4050 6750 4050
Wire Wire Line
	6750 3150 7100 3150
$Comp
L Device:R R10
U 1 1 61C18D39
P 6600 3150
F 0 "R10" V 6500 3150 50  0000 C CNN
F 1 "10K" V 6700 3150 50  0000 C CNN
F 2 "" V 6530 3150 50  0001 C CNN
F 3 "~" H 6600 3150 50  0001 C CNN
	1    6600 3150
	0    1    1    0   
$EndComp
$Comp
L Device:R R11
U 1 1 61C180BC
P 6600 4050
F 0 "R11" V 6500 4050 50  0000 C CNN
F 1 "10K" V 6700 4050 50  0000 C CNN
F 2 "" V 6530 4050 50  0001 C CNN
F 3 "~" H 6600 4050 50  0001 C CNN
	1    6600 4050
	0    1    1    0   
$EndComp
$Comp
L Transistor_BJT:S8050 Q2
U 1 1 61C176D1
P 7300 4050
F 0 "Q2" H 7490 4004 50  0000 L CNN
F 1 "S8050" H 7490 4095 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 7500 3975 50  0001 L CIN
F 3 "http://www.unisonic.com.tw/datasheet/S8050.pdf" H 7300 4050 50  0001 L CNN
	1    7300 4050
	1    0    0    1   
$EndComp
$Comp
L Transistor_BJT:S8050 Q1
U 1 1 61C164BA
P 7300 3150
F 0 "Q1" H 7490 3196 50  0000 L CNN
F 1 "S8050" H 7490 3105 50  0000 L CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 7500 3075 50  0001 L CIN
F 3 "http://www.unisonic.com.tw/datasheet/S8050.pdf" H 7300 3150 50  0001 L CNN
	1    7300 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 4850 5750 4850
Wire Wire Line
	4850 4250 5750 4250
$Comp
L power:Earth #PWR09
U 1 1 61C0403A
P 5750 4850
F 0 "#PWR09" H 5750 4600 50  0001 C CNN
F 1 "Earth" H 5750 4700 50  0001 C CNN
F 2 "" H 5750 4850 50  0001 C CNN
F 3 "~" H 5750 4850 50  0001 C CNN
	1    5750 4850
	0    -1   -1   0   
$EndComp
$Comp
L power:Earth #PWR08
U 1 1 61C035EE
P 5750 4250
F 0 "#PWR08" H 5750 4000 50  0001 C CNN
F 1 "Earth" H 5750 4100 50  0001 C CNN
F 2 "" H 5750 4250 50  0001 C CNN
F 3 "~" H 5750 4250 50  0001 C CNN
	1    5750 4250
	0    -1   -1   0   
$EndComp
Text Label 3450 4850 0    50   ~ 0
VDD3V3
Text Label 3450 4250 0    50   ~ 0
IO2
Wire Wire Line
	3450 4850 3950 4850
Wire Wire Line
	3450 4250 3900 4250
Wire Wire Line
	4250 4850 4550 4850
Wire Wire Line
	4200 4250 4550 4250
$Comp
L Device:R R9
U 1 1 61BE9FAD
P 4100 4850
F 0 "R9" V 4050 4700 50  0000 C CNN
F 1 "1K" V 4050 5000 50  0000 C CNN
F 2 "" V 4030 4850 50  0001 C CNN
F 3 "~" H 4100 4850 50  0001 C CNN
	1    4100 4850
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 61BE8549
P 4050 4250
F 0 "R5" V 4000 4100 50  0000 C CNN
F 1 "1K" V 4000 4400 50  0000 C CNN
F 2 "" V 3980 4250 50  0001 C CNN
F 3 "~" H 4050 4250 50  0001 C CNN
	1    4050 4250
	0    1    1    0   
$EndComp
$Comp
L Device:LED D2
U 1 1 61BE7B61
P 4700 4850
F 0 "D2" H 4693 5067 50  0000 C CNN
F 1 "LED" H 4693 4976 50  0000 C CNN
F 2 "" H 4700 4850 50  0001 C CNN
F 3 "~" H 4700 4850 50  0001 C CNN
	1    4700 4850
	-1   0    0    -1  
$EndComp
$Comp
L Device:LED D1
U 1 1 61BE3DDE
P 4700 4250
F 0 "D1" H 4693 4467 50  0000 C CNN
F 1 "LED" H 4693 4376 50  0000 C CNN
F 2 "" H 4700 4250 50  0001 C CNN
F 3 "~" H 4700 4250 50  0001 C CNN
	1    4700 4250
	-1   0    0    -1  
$EndComp
Text Label 2100 6000 0    50   ~ 0
EN
Wire Wire Line
	2650 6300 2800 6300
Connection ~ 2650 6300
Wire Wire Line
	2650 6650 2650 6300
Wire Wire Line
	2850 6650 2650 6650
Wire Wire Line
	3600 6650 3600 6850
Connection ~ 3600 6650
Wire Wire Line
	3150 6650 3600 6650
Wire Wire Line
	3600 6300 3600 6650
Wire Wire Line
	3200 6300 3600 6300
$Comp
L Device:C C5
U 1 1 61BDD27A
P 3000 6650
F 0 "C5" H 3115 6696 50  0000 L CNN
F 1 "1nF" H 3115 6605 50  0000 L CNN
F 2 "" H 3038 6500 50  0001 C CNN
F 3 "~" H 3000 6650 50  0001 C CNN
	1    3000 6650
	0    1    1    0   
$EndComp
Wire Wire Line
	2500 6300 2650 6300
Wire Wire Line
	2100 6300 2200 6300
Connection ~ 2100 6300
Wire Wire Line
	2100 6000 2100 6300
Wire Wire Line
	2000 6300 2100 6300
$Comp
L Device:R R8
U 1 1 61BDD26F
P 2350 6300
F 0 "R8" V 2250 6300 50  0000 C CNN
F 1 "470" V 2450 6300 50  0000 C CNN
F 2 "" V 2280 6300 50  0001 C CNN
F 3 "~" H 2350 6300 50  0001 C CNN
	1    2350 6300
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 61BDD269
P 1850 6300
F 0 "R7" V 1750 6300 50  0000 C CNN
F 1 "10K" V 1950 6300 50  0000 C CNN
F 2 "" V 1780 6300 50  0001 C CNN
F 3 "~" H 1850 6300 50  0001 C CNN
	1    1850 6300
	0    1    1    0   
$EndComp
Text Label 1100 6300 0    50   ~ 0
VDD3V3
Wire Wire Line
	1700 6300 1100 6300
$Comp
L power:Earth #PWR07
U 1 1 61BDD261
P 3600 6850
F 0 "#PWR07" H 3600 6600 50  0001 C CNN
F 1 "Earth" H 3600 6700 50  0001 C CNN
F 2 "" H 3600 6850 50  0001 C CNN
F 3 "~" H 3600 6850 50  0001 C CNN
	1    3600 6850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 61BDD25B
P 3000 6300
F 0 "SW3" H 3000 6585 50  0000 C CNN
F 1 "EN" H 3000 6494 50  0000 C CNN
F 2 "" H 3000 6500 50  0001 C CNN
F 3 "~" H 3000 6500 50  0001 C CNN
	1    3000 6300
	1    0    0    -1  
$EndComp
Text Label 2100 4750 0    50   ~ 0
IO0
Wire Wire Line
	2650 5050 2800 5050
Connection ~ 2650 5050
Wire Wire Line
	2650 5400 2650 5050
Wire Wire Line
	2850 5400 2650 5400
Wire Wire Line
	3600 5400 3600 5600
Connection ~ 3600 5400
Wire Wire Line
	3150 5400 3600 5400
Wire Wire Line
	3600 5050 3600 5400
Wire Wire Line
	3200 5050 3600 5050
$Comp
L Device:C C4
U 1 1 61BBAD66
P 3000 5400
F 0 "C4" H 3115 5446 50  0000 L CNN
F 1 "1nF" H 3115 5355 50  0000 L CNN
F 2 "" H 3038 5250 50  0001 C CNN
F 3 "~" H 3000 5400 50  0001 C CNN
	1    3000 5400
	0    1    1    0   
$EndComp
Wire Wire Line
	2500 5050 2650 5050
Wire Wire Line
	2100 5050 2200 5050
Connection ~ 2100 5050
Wire Wire Line
	2100 4750 2100 5050
Wire Wire Line
	2000 5050 2100 5050
$Comp
L Device:R R6
U 1 1 61BAD8AF
P 2350 5050
F 0 "R6" V 2250 5050 50  0000 C CNN
F 1 "470" V 2450 5050 50  0000 C CNN
F 2 "" V 2280 5050 50  0001 C CNN
F 3 "~" H 2350 5050 50  0001 C CNN
	1    2350 5050
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 61BAD5C5
P 1850 5050
F 0 "R4" V 1750 5050 50  0000 C CNN
F 1 "10K" V 1950 5050 50  0000 C CNN
F 2 "" V 1780 5050 50  0001 C CNN
F 3 "~" H 1850 5050 50  0001 C CNN
	1    1850 5050
	0    1    1    0   
$EndComp
Text Label 1100 5050 0    50   ~ 0
VDD3V3
Wire Wire Line
	1700 5050 1100 5050
$Comp
L power:Earth #PWR03
U 1 1 61BA71C8
P 3600 5600
F 0 "#PWR03" H 3600 5350 50  0001 C CNN
F 1 "Earth" H 3600 5450 50  0001 C CNN
F 2 "" H 3600 5600 50  0001 C CNN
F 3 "~" H 3600 5600 50  0001 C CNN
	1    3600 5600
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW2
U 1 1 61BA3A0E
P 3000 5050
F 0 "SW2" H 3000 5335 50  0000 C CNN
F 1 "BOOT" H 3000 5244 50  0000 C CNN
F 2 "" H 3000 5250 50  0001 C CNN
F 3 "~" H 3000 5250 50  0001 C CNN
	1    3000 5050
	1    0    0    -1  
$EndComp
Text Label 5650 7300 1    50   ~ 0
D4
Text Label 5550 7300 1    50   ~ 0
IO2
Text Label 5450 7300 1    50   ~ 0
D14
NoConn ~ 5350 7300
Text Label 5150 7300 1    50   ~ 0
D12
Text Label 6750 5550 0    50   ~ 0
D23
Text Label 6750 5650 0    50   ~ 0
D22
Text Label 6750 5750 0    50   ~ 0
TX0
Text Label 6750 5850 0    50   ~ 0
RX0
Text Label 6750 5950 0    50   ~ 0
D21
Text Label 6750 6150 0    50   ~ 0
D19
Text Label 6750 6250 0    50   ~ 0
D18
Text Label 6750 6350 0    50   ~ 0
D5
Text Label 6750 6450 0    50   ~ 0
D17
Text Label 6750 6550 0    50   ~ 0
D16
Text Label 6750 6650 0    50   ~ 0
D4
Text Label 6750 6750 0    50   ~ 0
IO0
Text Label 5850 7300 1    50   ~ 0
IO2
Text Label 5750 7300 1    50   ~ 0
D15
Text Label 5050 7300 1    50   ~ 0
D13
Text Label 3950 6750 0    50   ~ 0
D12
Text Label 3950 6650 0    50   ~ 0
D14
Text Label 3950 6550 0    50   ~ 0
D27
Text Label 3950 6450 0    50   ~ 0
D26
Text Label 3950 6350 0    50   ~ 0
D25
Text Label 3950 6250 0    50   ~ 0
D33
Text Label 3950 6150 0    50   ~ 0
D32
Text Label 3950 6050 0    50   ~ 0
D35
Text Label 3950 5950 0    50   ~ 0
D34
Text Label 3950 5850 0    50   ~ 0
VN
Text Label 3950 5750 0    50   ~ 0
VP
NoConn ~ 6300 6050
Wire Wire Line
	6500 5450 6500 5300
Wire Wire Line
	6300 5450 6500 5450
$Comp
L power:Earth #PWR06
U 1 1 61B921BC
P 6500 5300
F 0 "#PWR06" H 6500 5050 50  0001 C CNN
F 1 "Earth" H 6500 5150 50  0001 C CNN
F 2 "" H 6500 5300 50  0001 C CNN
F 3 "~" H 6500 5300 50  0001 C CNN
	1    6500 5300
	1    0    0    1   
$EndComp
Wire Wire Line
	6300 5550 6750 5550
Wire Wire Line
	6300 5750 6750 5750
Wire Wire Line
	6300 5650 6750 5650
Wire Wire Line
	6300 5850 6750 5850
Wire Wire Line
	6300 5950 6750 5950
Wire Wire Line
	6300 6150 6750 6150
Wire Wire Line
	6300 6250 6750 6250
Wire Wire Line
	6300 6350 6750 6350
Wire Wire Line
	6300 6450 6750 6450
Wire Wire Line
	6300 6550 6750 6550
Wire Wire Line
	6300 6650 6750 6650
Wire Wire Line
	6300 6750 6750 6750
Wire Wire Line
	5850 6900 5850 7300
Wire Wire Line
	5750 6900 5750 7300
Wire Wire Line
	5650 6900 5650 7300
Wire Wire Line
	5550 6900 5550 7300
Wire Wire Line
	5450 6900 5450 7300
Wire Wire Line
	5350 6900 5350 7300
Wire Wire Line
	5250 6900 5250 7300
Wire Wire Line
	5150 6900 5150 7300
Wire Wire Line
	5050 6900 5050 7300
Wire Wire Line
	4950 7300 4950 6900
$Comp
L power:Earth #PWR05
U 1 1 61B6F753
P 4950 7300
F 0 "#PWR05" H 4950 7050 50  0001 C CNN
F 1 "Earth" H 4950 7150 50  0001 C CNN
F 2 "" H 4950 7300 50  0001 C CNN
F 3 "~" H 4950 7300 50  0001 C CNN
	1    4950 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 6750 3950 6750
Wire Wire Line
	4500 6650 3950 6650
Wire Wire Line
	3950 6550 4500 6550
Wire Wire Line
	4500 6450 3950 6450
Wire Wire Line
	4500 6350 3950 6350
Wire Wire Line
	4500 6250 3950 6250
Wire Wire Line
	4500 6150 3950 6150
Wire Wire Line
	4500 6050 3950 6050
Wire Wire Line
	4500 5950 3950 5950
Wire Wire Line
	4500 5850 3950 5850
Wire Wire Line
	4500 5750 3950 5750
Text Label 3950 5650 0    50   ~ 0
EN
Wire Wire Line
	3950 5650 4500 5650
Text Label 3950 5550 0    50   ~ 0
VDD3V3
Wire Wire Line
	4500 5550 3950 5550
Wire Wire Line
	4350 5450 4350 5300
Wire Wire Line
	4500 5450 4350 5450
$Comp
L power:Earth #PWR04
U 1 1 61B5E7AA
P 4350 5300
F 0 "#PWR04" H 4350 5050 50  0001 C CNN
F 1 "Earth" H 4350 5150 50  0001 C CNN
F 2 "" H 4350 5300 50  0001 C CNN
F 3 "~" H 4350 5300 50  0001 C CNN
	1    4350 5300
	1    0    0    1   
$EndComp
$Comp
L My_Conn:ESP32 DD2
U 1 1 61B5C7E0
P 5400 5400
F 0 "DD2" H 5400 5575 50  0000 C CNN
F 1 "ESP-WROOM-32" H 5400 5484 50  0000 C CNN
F 2 "" H 5400 5500 50  0001 C CNN
F 3 "" H 5400 5500 50  0001 C CNN
	1    5400 5400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3550 2600 3750 2600
Wire Wire Line
	3550 2350 3550 2600
Text Label 5750 2800 0    50   ~ 0
RTS
Text Label 5750 2700 0    50   ~ 0
DTR
Wire Wire Line
	4750 2700 5750 2700
Wire Wire Line
	4750 2800 5750 2800
Text Label 5750 3000 0    50   ~ 0
RX0
Text Label 5750 2900 0    50   ~ 0
TX0
Wire Wire Line
	5450 3000 5750 3000
Wire Wire Line
	5450 2900 5750 2900
Text Label 5750 2300 0    50   ~ 0
VDD3V3
Wire Wire Line
	5750 2300 5450 2300
Wire Wire Line
	5150 2300 4750 2300
Wire Wire Line
	5150 2900 4750 2900
Wire Wire Line
	5150 3000 4750 3000
$Comp
L Device:R R3
U 1 1 61B52E08
P 5300 3000
F 0 "R3" V 5250 2850 50  0000 C CNN
F 1 "1K" V 5250 3150 50  0000 C CNN
F 2 "" V 5230 3000 50  0001 C CNN
F 3 "~" H 5300 3000 50  0001 C CNN
	1    5300 3000
	0    1    1    0   
$EndComp
$Comp
L Device:R R2
U 1 1 61B5287E
P 5300 2900
F 0 "R2" V 5250 2750 50  0000 C CNN
F 1 "1K" V 5250 3050 50  0000 C CNN
F 2 "" V 5230 2900 50  0001 C CNN
F 3 "~" H 5300 2900 50  0001 C CNN
	1    5300 2900
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 61B519A9
P 5300 2300
F 0 "R1" V 5250 2150 50  0000 C CNN
F 1 "4,7K" V 5250 2500 50  0000 C CNN
F 2 "" V 5230 2300 50  0001 C CNN
F 3 "~" H 5300 2300 50  0001 C CNN
	1    5300 2300
	0    1    1    0   
$EndComp
Text Label 3200 2300 0    50   ~ 0
VDD3V3
Wire Wire Line
	3600 2300 3750 2300
Connection ~ 3600 2300
Wire Wire Line
	3600 2400 3750 2400
Wire Wire Line
	3600 2300 3600 2400
Wire Wire Line
	3200 2300 3600 2300
Wire Wire Line
	4950 3700 4750 3700
Wire Wire Line
	4950 3850 4950 3700
$Comp
L power:Earth #PWR0109
U 1 1 61B47DA7
P 4950 3850
F 0 "#PWR0109" H 4950 3600 50  0001 C CNN
F 1 "Earth" H 4950 3700 50  0001 C CNN
F 2 "" H 4950 3850 50  0001 C CNN
F 3 "~" H 4950 3850 50  0001 C CNN
	1    4950 3850
	1    0    0    -1  
$EndComp
NoConn ~ 4750 2600
NoConn ~ 4750 2500
NoConn ~ 4750 2400
NoConn ~ 4750 3100
NoConn ~ 4750 3200
NoConn ~ 4750 3300
NoConn ~ 4750 3400
NoConn ~ 4750 3500
NoConn ~ 4750 3600
NoConn ~ 3750 3700
NoConn ~ 3750 3600
NoConn ~ 3750 3500
NoConn ~ 3750 3400
NoConn ~ 3750 3300
NoConn ~ 3750 3200
NoConn ~ 3750 3100
NoConn ~ 3750 3000
NoConn ~ 3750 2900
NoConn ~ 3750 2800
$Comp
L My_Conn:CP2102 DD3
U 1 1 61B3D23D
P 4250 2250
F 0 "DD3" H 3900 2300 50  0000 C CNN
F 1 "CP2102" H 4500 2300 50  0000 C CNN
F 2 "" H 4250 2300 50  0001 C CNN
F 3 "" H 4250 2300 50  0001 C CNN
	1    4250 2250
	1    0    0    -1  
$EndComp
Wire Notes Line
	900  1750 9850 1750
Wire Notes Line
	9850 7650 900  7650
Text Notes 1050 1850 0    50   ~ 0
А ESP32 DEVKITC V1
$Comp
L My_Conn:THT_MALE_1x15 XP2
U 1 1 61F0CAE5
P 8650 2750
F 0 "XP2" H 8708 2915 50  0000 C CNN
F 1 "THT_MALE_1x15" H 8708 2824 50  0000 C CNN
F 2 "" H 8650 2850 50  0001 C CNN
F 3 "" H 8650 2850 50  0001 C CNN
	1    8650 2750
	1    0    0    -1  
$EndComp
$Comp
L My_Conn:THT_MALE_1x15 XP3
U 1 1 61F0ED23
P 8650 5300
F 0 "XP3" H 8708 5465 50  0000 C CNN
F 1 "THT_MALE_1x15" H 8708 5374 50  0000 C CNN
F 2 "" H 8650 5400 50  0001 C CNN
F 3 "" H 8650 5400 50  0001 C CNN
	1    8650 5300
	1    0    0    -1  
$EndComp
$Comp
L My_Conn:ESP32DEVKITCV1 A2
U 1 1 61F20EEF
P 10950 4750
F 0 "A2" H 11250 4800 50  0000 C CNN
F 1 "ESP32DEVKITCV1" H 11150 1100 50  0000 C CNN
F 2 "" H 10950 4800 50  0001 C CNN
F 3 "" H 10950 4800 50  0001 C CNN
	1    10950 4750
	1    0    0    -1  
$EndComp
Text Label 12600 8200 2    50   ~ 0
VDD3V3
Text Label 12600 8100 2    50   ~ 0
GND
Text Label 12600 8000 2    50   ~ 0
D15
Text Label 12600 7900 2    50   ~ 0
IO2
Text Label 12600 7800 2    50   ~ 0
D4
Text Label 12600 7700 2    50   ~ 0
D16
Text Label 12600 7600 2    50   ~ 0
D17
Text Label 12600 7500 2    50   ~ 0
D5
Text Label 12600 7400 2    50   ~ 0
D18
Text Label 12600 7300 2    50   ~ 0
D19
Text Label 12600 7200 2    50   ~ 0
D21
Text Label 12600 7100 2    50   ~ 0
RX0
Text Label 12600 7000 2    50   ~ 0
TX0
Text Label 12600 6900 2    50   ~ 0
D22
Text Label 12600 6800 2    50   ~ 0
D23
Wire Wire Line
	12600 8200 12250 8200
Wire Wire Line
	12250 8100 12600 8100
Wire Wire Line
	12250 8000 12600 8000
Wire Wire Line
	12250 7900 12600 7900
Wire Wire Line
	12250 7800 12600 7800
Wire Wire Line
	12250 7700 12600 7700
Wire Wire Line
	12600 7600 12250 7600
Wire Wire Line
	12250 7500 12600 7500
Wire Wire Line
	12600 7400 12250 7400
Wire Wire Line
	12250 7300 12600 7300
Wire Wire Line
	12250 7100 12600 7100
Wire Wire Line
	12250 7000 12600 7000
Wire Wire Line
	12250 6800 12600 6800
Text Label 12600 6400 2    50   ~ 0
GND
Text Label 12600 6300 2    50   ~ 0
VIN
Text Label 12600 6200 2    50   ~ 0
D13
Text Label 12600 6100 2    50   ~ 0
D12
Text Label 12600 6000 2    50   ~ 0
D14
Text Label 12600 5900 2    50   ~ 0
D27
Text Label 12600 5800 2    50   ~ 0
D26
Text Label 12600 5700 2    50   ~ 0
D25
Text Label 12600 5600 2    50   ~ 0
D33
Text Label 12600 5500 2    50   ~ 0
D32
Text Label 12600 5400 2    50   ~ 0
D35
Text Label 12600 5300 2    50   ~ 0
D34
Text Label 12600 5200 2    50   ~ 0
VN
Text Label 12600 5100 2    50   ~ 0
VP
Text Label 12600 5000 2    50   ~ 0
EN
Wire Wire Line
	12600 6400 12250 6400
Wire Wire Line
	12250 6200 12600 6200
Wire Wire Line
	12250 6100 12600 6100
Wire Wire Line
	12250 5900 12600 5900
Wire Wire Line
	12600 5800 12250 5800
Wire Wire Line
	12250 5700 12600 5700
Wire Wire Line
	12600 5600 12250 5600
Wire Wire Line
	12250 5500 12600 5500
Wire Wire Line
	12250 5400 12600 5400
Wire Wire Line
	12250 5300 12600 5300
Wire Wire Line
	12250 5200 12600 5200
Wire Wire Line
	12250 5100 12600 5100
Wire Wire Line
	12250 5000 12600 5000
$Comp
L My_Conn:ESP32DEVKITCV1 A1
U 1 1 61FB43D7
P 10850 650
F 0 "A1" H 11200 700 50  0000 C CNN
F 1 "ESP32DEVKITCV1" H 11250 -3000 50  0000 C CNN
F 2 "" H 10850 700 50  0001 C CNN
F 3 "" H 10850 700 50  0001 C CNN
	1    10850 650 
	1    0    0    -1  
$EndComp
Wire Wire Line
	12150 2800 12500 2800
Wire Wire Line
	12150 3100 12500 3100
Text Label 12500 4100 2    50   ~ 0
VDD3V3
Text Label 12500 4000 2    50   ~ 0
GND
Text Label 12500 3900 2    50   ~ 0
D15
Text Label 12500 3800 2    50   ~ 0
IO2
Text Label 12500 3700 2    50   ~ 0
D4
Text Label 12500 3600 2    50   ~ 0
D16
Text Label 12500 3500 2    50   ~ 0
D17
Text Label 12500 3400 2    50   ~ 0
D5
Text Label 12500 3300 2    50   ~ 0
D18
Text Label 12500 3200 2    50   ~ 0
D19
Text Label 12500 3100 2    50   ~ 0
D21
Text Label 12500 3000 2    50   ~ 0
RX0
Text Label 12500 2900 2    50   ~ 0
TX0
Text Label 12500 2800 2    50   ~ 0
D22
Text Label 12500 2700 2    50   ~ 0
D23
Wire Wire Line
	12150 4000 12500 4000
Wire Wire Line
	12150 3900 12500 3900
Wire Wire Line
	12150 3800 12500 3800
Wire Wire Line
	12150 3600 12500 3600
Wire Wire Line
	12500 3500 12150 3500
Wire Wire Line
	12150 3400 12500 3400
Wire Wire Line
	12500 3300 12150 3300
Wire Wire Line
	12150 3200 12500 3200
Wire Wire Line
	12150 3000 12500 3000
Wire Wire Line
	12150 2900 12500 2900
Wire Wire Line
	12150 2700 12500 2700
Wire Wire Line
	12150 2200 12500 2200
Text Label 12500 2300 2    50   ~ 0
GND
Text Label 12500 2200 2    50   ~ 0
VIN
Text Label 12500 2100 2    50   ~ 0
D13
Text Label 12500 2000 2    50   ~ 0
D12
Text Label 12500 1900 2    50   ~ 0
D14
Text Label 12500 1800 2    50   ~ 0
D27
Text Label 12500 1700 2    50   ~ 0
D26
Text Label 12500 1600 2    50   ~ 0
D25
Text Label 12500 1500 2    50   ~ 0
D33
Text Label 12500 1400 2    50   ~ 0
D32
Text Label 12500 1300 2    50   ~ 0
D35
Text Label 12500 1200 2    50   ~ 0
D34
Text Label 12500 1100 2    50   ~ 0
VN
Text Label 12500 1000 2    50   ~ 0
VP
Text Label 12500 900  2    50   ~ 0
EN
Wire Wire Line
	12150 2100 12500 2100
Wire Wire Line
	12150 2000 12500 2000
Wire Wire Line
	12150 1900 12500 1900
Wire Wire Line
	12150 1800 12500 1800
Wire Wire Line
	12500 1700 12150 1700
Wire Wire Line
	12150 1600 12500 1600
Wire Wire Line
	12500 1500 12150 1500
Wire Wire Line
	12150 1400 12500 1400
Wire Wire Line
	12150 1300 12500 1300
Wire Wire Line
	12150 1200 12500 1200
Wire Wire Line
	12150 1100 12500 1100
Wire Wire Line
	12150 1000 12500 1000
Wire Wire Line
	12150 900  12500 900 
Wire Wire Line
	13700 8400 13700 7100
Wire Wire Line
	13700 7100 14850 7100
Wire Wire Line
	14850 7100 14850 6800
Wire Wire Line
	14500 6800 14850 6800
Wire Wire Line
	13850 8200 13850 7200
Wire Wire Line
	13850 7200 14550 7200
Wire Wire Line
	14550 7200 14550 6600
Wire Wire Line
	13900 8100 13900 7250
Wire Wire Line
	13900 7250 14600 7250
Wire Wire Line
	14600 7250 14600 6500
Wire Wire Line
	14400 6500 14600 6500
Wire Wire Line
	13800 8000 13800 7300
Wire Wire Line
	13800 7300 14650 7300
Wire Wire Line
	14650 7300 14650 6400
Wire Wire Line
	14350 6400 14650 6400
Wire Wire Line
	13750 7900 13750 7150
Wire Wire Line
	13750 7150 14700 7150
Wire Wire Line
	14700 7150 14700 6300
Wire Wire Line
	14300 6300 14700 6300
Wire Wire Line
	14050 7000 14750 7000
Wire Wire Line
	14750 7000 14750 6200
Wire Wire Line
	14250 6200 14750 6200
Wire Wire Line
	14050 7000 14050 7800
Wire Wire Line
	14100 6950 14900 6950
Wire Wire Line
	14900 6950 14900 6100
Wire Wire Line
	14200 6100 14900 6100
Wire Wire Line
	14150 7700 14100 7700
Wire Wire Line
	14150 7600 14150 6900
Wire Wire Line
	14150 6900 15000 6900
Wire Wire Line
	15000 6900 15000 6000
Wire Wire Line
	14150 6000 15000 6000
Wire Notes Line
	16300 8750 9950 8750
Text Notes 14250 4750 0    50   ~ 0
\n\nПринципиальная схема для работы с дисплеем\n
Wire Wire Line
	13600 4750 13850 4750
Wire Wire Line
	13600 4750 13600 4900
$Comp
L Sensor:DHT11 DD4
U 1 1 62355DA7
P 13950 1050
F 0 "DD4" H 13706 1096 50  0000 R CNN
F 1 "DHT11" H 13706 1005 50  0000 R CNN
F 2 "Sensor:Aosong_DHT11_5.5x12.0_P2.54mm" H 13950 650 50  0001 C CNN
F 3 "http://akizukidenshi.com/download/ds/aosong/DHT11.pdf" H 14100 1300 50  0001 C CNN
	1    13950 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	13950 1350 13950 2300
Wire Wire Line
	12150 2300 13950 2300
Wire Wire Line
	13950 750  12850 750 
Wire Wire Line
	12850 750  12850 4100
Wire Wire Line
	12150 4100 12850 4100
Wire Notes Line
	16200 400  16200 4450
Wire Notes Line
	16200 4450 9950 4450
Wire Notes Line
	9950 4450 9950 400 
Wire Notes Line
	9950 400  16200 400 
Text Notes 13900 500  0    50   ~ 0
\nПринципиальная схема для работы с датчиком DHT11\n
Wire Wire Line
	14250 1050 14250 3700
Wire Wire Line
	12150 3700 14250 3700
Wire Notes Line
	9850 7650 9850 1750
Wire Notes Line
	900  1750 900  7650
Text Notes 7300 1900 0    50   ~ 0
Принципиальная схема отладочного модуля ESP32 DEVKITC V1\n
Wire Notes Line
	9950 4550 16300 4550
Wire Notes Line
	16300 4550 16300 8750
Wire Notes Line
	9950 4550 9950 8750
Text Notes 1850 2800 0    50   ~ 0
Micro-USB\n
Text Notes 10100 1650 0    50   ~ 0
Micro-USB\n
Text Notes 10200 5750 0    50   ~ 0
Micro-USB\n
Wire Wire Line
	12250 6300 12650 6300
Wire Wire Line
	12850 5400 12850 5200
Connection ~ 12850 5400
Wire Wire Line
	12250 6000 12600 6000
Wire Wire Line
	12650 6300 12650 6700
Wire Wire Line
	12650 6700 12950 6700
Wire Wire Line
	12950 6700 12950 7300
Wire Wire Line
	12250 6900 12750 6900
Wire Wire Line
	12750 6900 12750 6000
Wire Wire Line
	12750 6000 13100 6000
Wire Wire Line
	12700 7200 12700 6100
Wire Wire Line
	12250 7200 12700 7200
Wire Wire Line
	12700 6100 13100 6100
Wire Wire Line
	13750 7900 14150 7900
Wire Wire Line
	13800 8000 14150 8000
Wire Wire Line
	13900 8100 14150 8100
Wire Wire Line
	13850 8200 14150 8200
Wire Wire Line
	13700 8400 14150 8400
Wire Wire Line
	14050 7800 14150 7800
Wire Wire Line
	14100 6950 14100 7700
$EndSCHEMATC
