EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Лабораторная работа 4"
Date "2021-12-22"
Rev ""
Comp "МГТУ. им. Баумана"
Comment1 ""
Comment2 "Баазова"
Comment3 "Хохлов С. А."
Comment4 ""
$EndDescr
$Comp
L Device:R R1
U 1 1 6178D821
P 3250 2900
F 0 "R1" V 3150 2900 50  0000 C CNN
F 1 "1k" V 3325 2900 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 3180 2900 50  0001 C CNN
F 3 "~" H 3250 2900 50  0001 C CNN
	1    3250 2900
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 6179A6D2
P 2850 3600
F 0 "C1" V 2775 3725 50  0000 C CNN
F 1 "20p" V 2950 3725 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2888 3450 50  0001 C CNN
F 3 "~" H 2850 3600 50  0001 C CNN
F 4 "Конденсаторы" H 2850 3600 60  0001 C CNN "Группа"
	1    2850 3600
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 6179BA14
P 2850 4100
F 0 "C2" V 2775 4225 50  0000 C CNN
F 1 "20p" V 3000 4125 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2888 3950 50  0001 C CNN
F 3 "~" H 2850 4100 50  0001 C CNN
	1    2850 4100
	0    1    1    0   
$EndComp
Wire Wire Line
	3250 3700 3250 3600
Wire Wire Line
	3250 3600 3000 3600
Wire Wire Line
	3250 4100 3250 4000
Wire Wire Line
	3000 4100 3250 4100
Wire Wire Line
	3950 3600 3250 3600
Connection ~ 3250 3600
Wire Wire Line
	3250 4100 3950 4100
Wire Wire Line
	3950 4100 3950 3800
Connection ~ 3250 4100
Wire Wire Line
	2150 4100 2700 4100
Wire Wire Line
	2700 3600 2150 3600
Connection ~ 2150 3600
Wire Wire Line
	2150 3600 2150 4100
Wire Wire Line
	2150 3400 2150 3600
Text Notes 3700 4075 0    50   ~ 0
XTAL2
Text Notes 3675 3575 0    50   ~ 0
XTAL1
Text Notes 3700 3375 0    50   ~ 0
Reset
Wire Wire Line
	7150 8800 7150 8900
Wire Wire Line
	7150 8800 7150 8700
Connection ~ 7150 8800
Wire Wire Line
	6550 8800 7150 8800
Wire Wire Line
	6850 8400 6550 8400
$Comp
L Device:C C3
U 1 1 61E94B49
P 6550 8600
F 0 "C3" H 6475 8725 50  0000 C CNN
F 1 "0.33u" H 6450 8500 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 6588 8450 50  0001 C CNN
F 3 "~" H 6550 8600 50  0001 C CNN
	1    6550 8600
	1    0    0    -1  
$EndComp
$Comp
L Device:C C4
U 1 1 61E9F931
P 7750 8600
F 0 "C4" H 7825 8725 50  0000 C CNN
F 1 "0.1u" H 7925 8525 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7788 8450 50  0001 C CNN
F 3 "~" H 7750 8600 50  0001 C CNN
	1    7750 8600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 61D234D0
P 7150 8900
F 0 "#PWR0101" H 7150 8650 50  0001 C CNN
F 1 "GND" H 7155 8727 50  0000 C CNN
F 2 "" H 7150 8900 50  0001 C CNN
F 3 "" H 7150 8900 50  0001 C CNN
	1    7150 8900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 7100 4550 7200
$Comp
L power:GND #PWR0102
U 1 1 61D77A79
P 4550 7200
F 0 "#PWR0102" H 4550 6950 50  0001 C CNN
F 1 "GND" H 4555 7027 50  0000 C CNN
F 2 "" H 4550 7200 50  0001 C CNN
F 3 "" H 4550 7200 50  0001 C CNN
	1    4550 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3400 2150 3300
$Comp
L power:GND #PWR0105
U 1 1 61DC1D35
P 2150 3300
F 0 "#PWR0105" H 2150 3050 50  0001 C CNN
F 1 "GND" H 2155 3127 50  0000 C CNN
F 2 "" H 2150 3300 50  0001 C CNN
F 3 "" H 2150 3300 50  0001 C CNN
	1    2150 3300
	1    0    0    1   
$EndComp
Connection ~ 2150 3400
$Comp
L lab4-rescue:ConnectorGOST-Valve XP1
U 1 1 61E0403E
P 5300 8300
F 0 "XP1" H 5828 8246 50  0000 L CNN
F 1 "Разъём питания" H 5828 8155 50  0000 L CNN
F 2 "Connector:Banana_Jack_2Pin" H 5300 8700 50  0001 C CNN
F 3 "" H 5300 8700 50  0001 C CNN
	1    5300 8300
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7750 8450 7750 8400
Wire Wire Line
	7750 8750 7750 8800
Wire Wire Line
	6550 8800 6550 8750
Wire Wire Line
	6550 8450 6550 8400
Wire Wire Line
	6550 8400 5800 8400
Connection ~ 6550 8400
Wire Wire Line
	5800 8600 6050 8600
Wire Wire Line
	6050 8600 6050 8800
Wire Wire Line
	6050 8800 6550 8800
Connection ~ 6550 8800
$Comp
L power:VCC #PWR0107
U 1 1 61E8DB73
P 8300 8200
F 0 "#PWR0107" H 8300 8050 50  0001 C CNN
F 1 "VCC" H 8315 8373 50  0000 C CNN
F 2 "" H 8300 8200 50  0001 C CNN
F 3 "" H 8300 8200 50  0001 C CNN
	1    8300 8200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8300 8400 8300 8200
$Comp
L power:VCC #PWR0111
U 1 1 61EE8081
P 4500 2800
F 0 "#PWR0111" H 4500 2650 50  0001 C CNN
F 1 "VCC" H 4515 2973 50  0000 C CNN
F 2 "" H 4500 2800 50  0001 C CNN
F 3 "" H 4500 2800 50  0001 C CNN
	1    4500 2800
	1    0    0    -1  
$EndComp
$Comp
L Device:C C5
U 1 1 61CAA4A0
P 4800 3000
F 0 "C5" V 4548 3000 50  0000 C CNN
F 1 "0.1u" V 4639 3000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 4838 2850 50  0001 C CNN
F 3 "~" H 4800 3000 50  0001 C CNN
	1    4800 3000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0108
U 1 1 61CB18C3
P 5050 3000
F 0 "#PWR0108" H 5050 2750 50  0001 C CNN
F 1 "GND" H 5055 2827 50  0000 C CNN
F 2 "" H 5050 3000 50  0001 C CNN
F 3 "" H 5050 3000 50  0001 C CNN
	1    5050 3000
	0    -1   -1   0   
$EndComp
Wire Wire Line
	5050 3000 4950 3000
Wire Wire Line
	4650 3000 4500 3000
Connection ~ 7750 8400
Wire Wire Line
	7750 8400 8300 8400
Wire Wire Line
	7450 8400 7750 8400
Wire Wire Line
	7150 8800 7750 8800
$Comp
L Device:Crystal ZQ1
U 1 1 61D0EDCC
P 3250 3850
F 0 "ZQ1" V 3296 3719 50  0000 R CNN
F 1 "Crystal" V 3205 3719 50  0000 R CNN
F 2 "Crystal:Crystal_AT310_D3.0mm_L10.0mm_Horizontal" H 3250 3850 50  0001 C CNN
F 3 "~" H 3250 3850 50  0001 C CNN
	1    3250 3850
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Linear:L7805 U1
U 1 1 61D8DC52
P 7150 8400
F 0 "U1" H 7150 8642 50  0000 C CNN
F 1 "L7805" H 7150 8551 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-218-3_Vertical" H 7175 8250 50  0001 L CIN
F 3 "http://www.st.com/content/ccc/resource/technical/document/datasheet/41/4f/b3/b0/12/d4/47/88/CD00000444.pdf/files/CD00000444.pdf/jcr:content/translations/en.CD00000444.pdf" H 7150 8350 50  0001 C CNN
	1    7150 8400
	1    0    0    -1  
$EndComp
Connection ~ 4500 3000
Wire Wire Line
	4500 3000 4500 3100
$Comp
L Connector:AVR-ISP-6 XP2
U 1 1 61CECD73
P 7850 3200
F 0 "XP2" H 7521 3296 50  0000 R CNN
F 1 "Разъём программатора AVR-ISP-6" H 7521 3205 50  0000 R CNN
F 2 "Package_DIP:DIP-6_W7.62mm" V 7600 3250 50  0001 C CNN
F 3 " ~" H 6575 2650 50  0001 C CNN
	1    7850 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8250 3000 9450 3000
Wire Wire Line
	8250 3100 9450 3100
Wire Wire Line
	8250 3200 9450 3200
Entry Wire Line
	9450 3000 9550 3100
Entry Wire Line
	9450 3100 9550 3200
Wire Wire Line
	7750 3600 7750 3750
$Comp
L power:GND #PWR0104
U 1 1 61D2D6C6
P 7750 3750
F 0 "#PWR0104" H 7750 3500 50  0001 C CNN
F 1 "GND" H 7755 3577 50  0000 C CNN
F 2 "" H 7750 3750 50  0001 C CNN
F 3 "" H 7750 3750 50  0001 C CNN
	1    7750 3750
	1    0    0    -1  
$EndComp
Entry Wire Line
	9450 3200 9550 3300
Text Label 9200 3000 0    50   ~ 0
MISO
Text Label 9200 3100 0    50   ~ 0
MOSI
Text Label 9200 3200 0    50   ~ 0
SCK
Wire Wire Line
	4500 2800 4500 2900
Wire Wire Line
	2800 3400 3100 3400
Wire Wire Line
	8250 3300 8350 3300
Connection ~ 3650 3400
Wire Wire Line
	3650 3400 3950 3400
Wire Wire Line
	3400 2900 4500 2900
Connection ~ 4500 2900
Wire Wire Line
	4500 2900 4500 3000
Wire Wire Line
	3100 2900 3100 3400
Connection ~ 3100 3400
Wire Wire Line
	3100 3400 3650 3400
$Comp
L lab4-rescue:AT90S8515-MCU_Microchip_ATmega-E3-rescue DD1
U 1 1 61FC66B6
P 4550 5100
F 0 "DD1" H 4300 7150 50  0000 C CNN
F 1 "ATmega8515" H 5050 7050 50  0000 C CNN
F 2 "Package_QFP:TQFP-44_10x10mm_P0.8mm" H 4550 5100 50  0001 C CIN
F 3 "" H 4550 5100 50  0001 C CNN
	1    4550 5100
	1    0    0    -1  
$EndComp
$Comp
L Driver_LED:MAX7219 DD2
U 1 1 61FCBA50
P 7450 5950
F 0 "DD2" H 7250 6950 50  0000 C CNN
F 1 "MAX7219" H 7700 6950 50  0000 C CNN
F 2 "library:SOIC24" H 7400 6000 50  0001 C CNN
F 3 "https://datasheets.maximintegrated.com/en/ds/MAX7219-MAX7221.pdf" H 7500 5800 50  0001 C CNN
	1    7450 5950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 6550 6300 6550
Wire Wire Line
	7050 6650 6300 6650
Wire Wire Line
	7050 6750 6300 6750
Entry Wire Line
	6200 6450 6300 6550
Entry Wire Line
	6200 6550 6300 6650
Entry Wire Line
	6200 6650 6300 6750
Text Label 6350 6550 0    50   ~ 0
SS
Text Label 6350 6650 0    50   ~ 0
SCK
Text Label 6350 6750 0    50   ~ 0
MOSI
Wire Wire Line
	7450 6950 7450 7100
$Comp
L power:GND #PWR0103
U 1 1 61FD3108
P 7450 7100
F 0 "#PWR0103" H 7450 6850 50  0001 C CNN
F 1 "GND" H 7455 6927 50  0000 C CNN
F 2 "" H 7450 7100 50  0001 C CNN
F 3 "" H 7450 7100 50  0001 C CNN
	1    7450 7100
	1    0    0    -1  
$EndComp
Wire Wire Line
	7750 2500 7750 2700
$Comp
L power:VCC #PWR0109
U 1 1 61D358D8
P 7750 2500
F 0 "#PWR0109" H 7750 2350 50  0001 C CNN
F 1 "VCC" H 7765 2673 50  0000 C CNN
F 2 "" H 7750 2500 50  0001 C CNN
F 3 "" H 7750 2500 50  0001 C CNN
	1    7750 2500
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 61FD59AB
P 7450 4550
F 0 "#PWR0106" H 7450 4400 50  0001 C CNN
F 1 "VCC" H 7465 4723 50  0000 C CNN
F 2 "" H 7450 4550 50  0001 C CNN
F 3 "" H 7450 4550 50  0001 C CNN
	1    7450 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	8350 3300 8350 2250
Wire Wire Line
	3650 2250 3650 3400
Wire Bus Line
	6200 1650 9550 1650
Wire Wire Line
	3650 2250 8350 2250
Wire Wire Line
	10250 1950 9650 1950
Wire Wire Line
	10250 2050 9650 2050
Wire Wire Line
	10250 2150 9650 2150
Wire Wire Line
	10250 2250 9650 2250
Wire Wire Line
	10250 2350 9650 2350
Wire Wire Line
	10250 2450 9650 2450
Wire Wire Line
	10250 2550 9650 2550
Wire Wire Line
	10250 2650 9650 2650
Wire Wire Line
	12450 2350 13100 2350
Wire Wire Line
	12450 2450 13100 2450
Wire Wire Line
	12450 2550 13100 2550
Wire Wire Line
	12450 2650 13100 2650
Wire Wire Line
	12450 4100 13100 4100
Wire Wire Line
	12450 4200 13100 4200
Wire Wire Line
	7850 5150 9450 5150
Wire Wire Line
	7850 5250 9450 5250
Wire Wire Line
	7850 5350 9450 5350
Wire Wire Line
	7850 5450 9450 5450
Wire Wire Line
	7850 5550 9450 5550
Wire Wire Line
	7850 5650 9450 5650
Wire Wire Line
	7850 5750 9450 5750
Wire Wire Line
	7850 5850 9450 5850
Wire Wire Line
	7850 5950 9450 5950
Wire Wire Line
	7850 6050 9450 6050
Wire Wire Line
	7850 6150 9450 6150
Wire Wire Line
	7850 6250 9450 6250
Wire Wire Line
	7850 6350 9450 6350
Wire Wire Line
	7850 6450 9450 6450
Wire Wire Line
	7850 6550 9450 6550
Wire Wire Line
	7850 6650 9450 6650
NoConn ~ 7850 6750
Entry Wire Line
	9450 5150 9550 5250
Entry Wire Line
	9450 5250 9550 5350
Entry Wire Line
	9450 5350 9550 5450
Entry Wire Line
	9450 5450 9550 5550
Entry Wire Line
	9450 5550 9550 5650
Entry Wire Line
	9450 5650 9550 5750
Entry Wire Line
	9450 5750 9550 5850
Entry Wire Line
	9450 5850 9550 5950
Entry Wire Line
	9450 5950 9550 6050
Entry Wire Line
	9450 6050 9550 6150
Entry Wire Line
	9450 6150 9550 6250
Entry Wire Line
	9450 6250 9550 6350
Entry Wire Line
	9450 6350 9550 6450
Entry Wire Line
	9450 6450 9550 6550
Entry Wire Line
	9450 6550 9550 6650
Entry Wire Line
	9450 6650 9550 6750
Entry Wire Line
	13100 2550 13200 2650
Entry Wire Line
	13100 2650 13200 2750
Entry Wire Line
	13100 2350 13200 2450
Entry Wire Line
	13100 2450 13200 2550
Entry Wire Line
	13100 4100 13200 4200
Entry Wire Line
	13100 4200 13200 4300
$Comp
L Device:C C6
U 1 1 62065903
P 7750 4800
F 0 "C6" V 7498 4800 50  0000 C CNN
F 1 "0.1u" V 7589 4800 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 7788 4650 50  0001 C CNN
F 3 "~" H 7750 4800 50  0001 C CNN
	1    7750 4800
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 62065909
P 8000 4800
F 0 "#PWR0110" H 8000 4550 50  0001 C CNN
F 1 "GND" H 8005 4627 50  0000 C CNN
F 2 "" H 8000 4800 50  0001 C CNN
F 3 "" H 8000 4800 50  0001 C CNN
	1    8000 4800
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8000 4800 7900 4800
Wire Wire Line
	12450 3900 13100 3900
Wire Wire Line
	12450 4000 13100 4000
Entry Wire Line
	9550 1850 9650 1950
Entry Wire Line
	9550 1950 9650 2050
Entry Wire Line
	9550 2050 9650 2150
Entry Wire Line
	9550 2150 9650 2250
Entry Wire Line
	9550 2250 9650 2350
Entry Wire Line
	9550 2350 9650 2450
Entry Wire Line
	9550 2450 9650 2550
Entry Wire Line
	9550 2550 9650 2650
Entry Wire Line
	13100 3900 13200 4000
Entry Wire Line
	13100 4000 13200 4100
$Comp
L Device:R R2
U 1 1 620C5FB3
P 6750 5150
F 0 "R2" V 6650 5150 50  0000 C CNN
F 1 "10k" V 6825 5150 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 6680 5150 50  0001 C CNN
F 3 "~" H 6750 5150 50  0001 C CNN
	1    6750 5150
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 5150 7050 5150
Wire Wire Line
	6550 5150 6600 5150
Wire Wire Line
	6550 4650 7450 4650
Wire Wire Line
	7450 4650 7450 4550
Wire Wire Line
	6550 4650 6550 5150
Wire Wire Line
	7450 4950 7450 4800
Connection ~ 7450 4650
Wire Wire Line
	7600 4800 7450 4800
Connection ~ 7450 4800
Wire Wire Line
	7450 4800 7450 4650
Wire Wire Line
	5150 4700 6100 4700
Wire Wire Line
	5150 4800 6100 4800
Wire Wire Line
	5150 4900 6100 4900
Wire Wire Line
	5150 5000 6100 5000
Entry Wire Line
	6100 4700 6200 4800
Entry Wire Line
	6100 4800 6200 4900
Entry Wire Line
	6100 4900 6200 5000
Entry Wire Line
	6100 5000 6200 5100
$Comp
L Switch:SW_Push SW2
U 1 1 621033EF
P 10950 5350
F 0 "SW2" H 10950 5635 50  0000 C CNN
F 1 "PLUS" H 10950 5544 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10950 5550 50  0001 C CNN
F 3 "~" H 10950 5550 50  0001 C CNN
	1    10950 5350
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 6210714C
P 10950 5950
F 0 "SW3" H 10950 6235 50  0000 C CNN
F 1 "MINUS" H 10950 6144 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10950 6150 50  0001 C CNN
F 3 "~" H 10950 6150 50  0001 C CNN
	1    10950 5950
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 6210C2D0
P 10950 6450
F 0 "SW4" H 10950 6735 50  0000 C CNN
F 1 "YEAR" H 10950 6644 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10950 6650 50  0001 C CNN
F 3 "~" H 10950 6650 50  0001 C CNN
	1    10950 6450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 62111773
P 10950 6950
F 0 "SW5" H 10950 7235 50  0000 C CNN
F 1 "MONTH" H 10950 7144 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10950 7150 50  0001 C CNN
F 3 "~" H 10950 7150 50  0001 C CNN
	1    10950 6950
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW6
U 1 1 62116C0E
P 10950 7450
F 0 "SW6" H 10950 7735 50  0000 C CNN
F 1 "ENTER" H 10950 7644 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 10950 7650 50  0001 C CNN
F 3 "~" H 10950 7650 50  0001 C CNN
	1    10950 7450
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 62122A39
P 2600 3400
F 0 "SW1" H 2600 3685 50  0000 C CNN
F 1 "RESET" H 2600 3594 50  0000 C CNN
F 2 "Button_Switch_THT:SW_PUSH_6mm" H 2600 3600 50  0001 C CNN
F 3 "~" H 2600 3600 50  0001 C CNN
	1    2600 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3400 2400 3400
$Comp
L Device:R R3
U 1 1 6213F4DE
P 10200 5200
F 0 "R3" V 10100 5200 50  0000 C CNN
F 1 "1k" V 10275 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10130 5200 50  0001 C CNN
F 3 "~" H 10200 5200 50  0001 C CNN
	1    10200 5200
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 62144C18
P 10200 5800
F 0 "R4" V 10100 5800 50  0000 C CNN
F 1 "1k" V 10275 5800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10130 5800 50  0001 C CNN
F 3 "~" H 10200 5800 50  0001 C CNN
	1    10200 5800
	0    1    1    0   
$EndComp
$Comp
L Device:R R5
U 1 1 62149F72
P 10200 6300
F 0 "R5" V 10100 6300 50  0000 C CNN
F 1 "1k" V 10275 6300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10130 6300 50  0001 C CNN
F 3 "~" H 10200 6300 50  0001 C CNN
	1    10200 6300
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 6214F38A
P 10200 6800
F 0 "R6" V 10100 6800 50  0000 C CNN
F 1 "1k" V 10275 6800 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10130 6800 50  0001 C CNN
F 3 "~" H 10200 6800 50  0001 C CNN
	1    10200 6800
	0    1    1    0   
$EndComp
$Comp
L Device:R R7
U 1 1 62154709
P 10200 7300
F 0 "R7" V 10100 7300 50  0000 C CNN
F 1 "1k" V 10275 7300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric" V 10130 7300 50  0001 C CNN
F 3 "~" H 10200 7300 50  0001 C CNN
	1    10200 7300
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0112
U 1 1 621622C7
P 10500 4950
F 0 "#PWR0112" H 10500 4800 50  0001 C CNN
F 1 "VCC" H 10515 5123 50  0000 C CNN
F 2 "" H 10500 4950 50  0001 C CNN
F 3 "" H 10500 4950 50  0001 C CNN
	1    10500 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 7300 10500 7300
Wire Wire Line
	10500 4950 10500 5200
Wire Wire Line
	10350 5200 10500 5200
Connection ~ 10500 5200
Wire Wire Line
	10500 5200 10500 5800
Wire Wire Line
	10350 5800 10500 5800
Connection ~ 10500 5800
Wire Wire Line
	10500 5800 10500 6300
Wire Wire Line
	10350 6300 10500 6300
Connection ~ 10500 6300
Wire Wire Line
	10500 6300 10500 6800
Wire Wire Line
	10350 6800 10500 6800
Connection ~ 10500 6800
Wire Wire Line
	10500 6800 10500 7300
Wire Wire Line
	10050 5200 9950 5200
Wire Wire Line
	9650 5800 9950 5800
Wire Wire Line
	10050 6300 9950 6300
Wire Wire Line
	10050 6800 9950 6800
Wire Wire Line
	10050 7300 9950 7300
Wire Wire Line
	10750 5350 9950 5350
Wire Wire Line
	9950 5350 9950 5200
Connection ~ 9950 5200
Wire Wire Line
	9950 5200 9650 5200
Wire Wire Line
	10750 5950 9950 5950
Wire Wire Line
	9950 5950 9950 5800
Connection ~ 9950 5800
Wire Wire Line
	9950 5800 10050 5800
Wire Wire Line
	10750 6450 9950 6450
Wire Wire Line
	9950 6450 9950 6300
Connection ~ 9950 6300
Wire Wire Line
	9950 6300 9650 6300
Wire Wire Line
	10750 6950 9950 6950
Wire Wire Line
	9950 6950 9950 6800
Connection ~ 9950 6800
Wire Wire Line
	9950 6800 9650 6800
Wire Wire Line
	10750 7450 9950 7450
Wire Wire Line
	9950 7450 9950 7300
Connection ~ 9950 7300
Wire Wire Line
	9950 7300 9650 7300
Entry Wire Line
	9550 5100 9650 5200
Entry Wire Line
	9550 5700 9650 5800
Entry Wire Line
	9550 6200 9650 6300
Entry Wire Line
	9550 6700 9650 6800
Entry Wire Line
	9550 7200 9650 7300
$Comp
L power:GND #PWR0113
U 1 1 621D2EBB
P 11350 7600
F 0 "#PWR0113" H 11350 7350 50  0001 C CNN
F 1 "GND" H 11355 7427 50  0000 C CNN
F 2 "" H 11350 7600 50  0001 C CNN
F 3 "" H 11350 7600 50  0001 C CNN
	1    11350 7600
	1    0    0    -1  
$EndComp
Wire Wire Line
	11150 7450 11350 7450
Wire Wire Line
	11350 7450 11350 7600
Wire Wire Line
	11150 6950 11350 6950
Wire Wire Line
	11350 6950 11350 7450
Connection ~ 11350 7450
Wire Wire Line
	11150 6450 11350 6450
Wire Wire Line
	11350 6450 11350 6950
Connection ~ 11350 6950
Wire Wire Line
	11150 5950 11350 5950
Wire Wire Line
	11350 5950 11350 6450
Connection ~ 11350 6450
Wire Wire Line
	11150 5350 11350 5350
Wire Wire Line
	11350 5350 11350 5950
Connection ~ 11350 5950
Text Label 9650 5200 0    50   ~ 0
PLUS
Text Label 9650 5800 0    50   ~ 0
MINUS
Text Label 9650 6300 0    50   ~ 0
YEAR
Text Label 9650 6800 0    50   ~ 0
MONTH
Text Label 9650 7300 0    50   ~ 0
ENTER
Wire Wire Line
	5150 5200 6100 5200
Wire Wire Line
	5150 5300 6100 5300
Wire Wire Line
	5150 5400 6100 5400
Wire Wire Line
	5150 5500 6100 5500
Wire Wire Line
	5150 5600 6100 5600
Entry Wire Line
	6100 5200 6200 5300
Entry Wire Line
	6100 5300 6200 5400
Entry Wire Line
	6100 5400 6200 5500
Entry Wire Line
	6100 5500 6200 5600
Entry Wire Line
	6100 5600 6200 5700
Text Label 5750 5200 0    50   ~ 0
PLUS
Text Label 5750 5300 0    50   ~ 0
MINUS
Text Label 5750 5400 0    50   ~ 0
YEAR
Text Label 5750 5500 0    50   ~ 0
MONTH
Text Label 5750 5600 0    50   ~ 0
ENTER
Text Label 12850 2350 0    50   ~ 0
DIG0
Text Label 12850 2450 0    50   ~ 0
DIG1
Text Label 12850 2550 0    50   ~ 0
DIG2
Text Label 12850 2650 0    50   ~ 0
DIG3
Text Label 12850 3900 0    50   ~ 0
DIG4
Text Label 12850 4000 0    50   ~ 0
DIG5
Text Label 12850 4100 0    50   ~ 0
DIG6
Text Label 12850 4200 0    50   ~ 0
DIG7
Text Label 9150 5950 0    50   ~ 0
DIG0
Text Label 9150 6050 0    50   ~ 0
DIG1
Text Label 9150 6150 0    50   ~ 0
DIG2
Text Label 9150 6250 0    50   ~ 0
DIG3
Text Label 9150 6350 0    50   ~ 0
DIG4
Text Label 9150 6450 0    50   ~ 0
DIG5
Text Label 9150 6550 0    50   ~ 0
DIG6
Text Label 9150 6650 0    50   ~ 0
DIG7
Text Label 9150 5850 0    50   ~ 0
DP
Text Label 9150 5750 0    50   ~ 0
G
Text Label 9150 5650 0    50   ~ 0
F
Text Label 9150 5550 0    50   ~ 0
E
Text Label 9150 5450 0    50   ~ 0
D
Text Label 9150 5350 0    50   ~ 0
C
Text Label 9150 5250 0    50   ~ 0
B
Text Label 9150 5150 0    50   ~ 0
A
NoConn ~ 5150 6800
NoConn ~ 5150 6700
NoConn ~ 5150 6600
NoConn ~ 5150 6500
NoConn ~ 5150 6400
NoConn ~ 5150 6300
NoConn ~ 5150 6200
NoConn ~ 5150 6100
NoConn ~ 5150 5900
NoConn ~ 5150 5800
NoConn ~ 5150 5700
NoConn ~ 5150 4600
NoConn ~ 5150 4500
NoConn ~ 5150 4400
NoConn ~ 5150 4300
NoConn ~ 5150 4100
NoConn ~ 5150 4000
NoConn ~ 5150 3900
NoConn ~ 5150 3800
NoConn ~ 5150 3700
NoConn ~ 5150 3600
NoConn ~ 5150 3500
NoConn ~ 5150 3400
Text Label 9750 2150 0    50   ~ 0
C
Text Label 9750 2250 0    50   ~ 0
D
Text Label 9750 2350 0    50   ~ 0
E
Text Label 9750 2450 0    50   ~ 0
F
Text Label 9750 2550 0    50   ~ 0
G
Text Label 9750 2650 0    50   ~ 0
DP
Text Label 9750 1950 0    50   ~ 0
A
Text Label 9750 2050 0    50   ~ 0
B
Wire Wire Line
	10250 3500 9650 3500
Wire Wire Line
	10250 3600 9650 3600
Wire Wire Line
	10250 3700 9650 3700
Wire Wire Line
	10250 3800 9650 3800
Wire Wire Line
	10250 3900 9650 3900
Wire Wire Line
	10250 4000 9650 4000
Wire Wire Line
	10250 4100 9650 4100
Wire Wire Line
	10250 4200 9650 4200
Entry Wire Line
	9550 3400 9650 3500
Entry Wire Line
	9550 3500 9650 3600
Entry Wire Line
	9550 3600 9650 3700
Entry Wire Line
	9550 3700 9650 3800
Entry Wire Line
	9550 3800 9650 3900
Entry Wire Line
	9550 3900 9650 4000
Entry Wire Line
	9550 4000 9650 4100
Entry Wire Line
	9550 4100 9650 4200
Text Label 9750 3700 0    50   ~ 0
C
Text Label 9750 3800 0    50   ~ 0
D
Text Label 9750 3900 0    50   ~ 0
E
Text Label 9750 4000 0    50   ~ 0
F
Text Label 9750 4100 0    50   ~ 0
G
Text Label 9750 4200 0    50   ~ 0
DP
Text Label 9750 3500 0    50   ~ 0
A
Text Label 9750 3600 0    50   ~ 0
B
Text Label 5750 4700 0    50   ~ 0
SS
Text Label 5750 4800 0    50   ~ 0
MOSI
Text Label 5750 4900 0    50   ~ 0
MISO
Text Label 5750 5000 0    50   ~ 0
SCK
$Comp
L Display_Character:CA56-12CGKWA HG1
U 1 1 61FED791
P 11350 2250
F 0 "HG1" H 11350 2917 50  0000 C CNN
F 1 "CA56-12CGKWA" H 11350 2826 50  0000 C CNN
F 2 "Display_7Segment:CA56-12CGKWA" H 11350 1650 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/CA56-12CGKWA(Ver.9A).pdf" H 10920 2280 50  0001 C CNN
	1    11350 2250
	1    0    0    -1  
$EndComp
$Comp
L Display_Character:CA56-12CGKWA HG2
U 1 1 61FEFE82
P 11350 3800
F 0 "HG2" H 11350 4467 50  0000 C CNN
F 1 "CA56-12CGKWA" H 11350 4376 50  0000 C CNN
F 2 "Display_7Segment:CA56-12CGKWA" H 11350 3200 50  0001 C CNN
F 3 "http://www.kingbright.com/attachments/file/psearch/000/00/00/CA56-12CGKWA(Ver.9A).pdf" H 10920 3830 50  0001 C CNN
	1    11350 3800
	1    0    0    -1  
$EndComp
Wire Bus Line
	9550 8500 13200 8500
Wire Bus Line
	6200 1650 6200 7000
Wire Bus Line
	13200 2000 13200 8500
Wire Bus Line
	9550 1650 9550 8500
$EndSCHEMATC
