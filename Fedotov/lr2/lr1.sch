EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 1 1
Title "Схема электрическая принципиальная"
Date ""
Rev ""
Comp "МГТУ им. Н.Э.Баумана"
Comment1 "Лабораторная работа 2"
Comment2 "Федотов Н.И"
Comment3 "Хохлов С.А"
Comment4 ""
$EndDescr
$Comp
L Device:R R2
U 1 1 61DFBEA3
P 10750 3100
F 0 "R2" V 10543 3100 50  0000 C CNN
F 1 "1 k" V 10634 3100 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 10680 3100 50  0001 C CNN
F 3 "~" H 10750 3100 50  0001 C CNN
	1    10750 3100
	0    1    1    0   
$EndComp
$Comp
L Device:R R1
U 1 1 61DFDA6B
P 10700 4750
F 0 "R1" V 10493 4750 50  0000 C CNN
F 1 "100" V 10584 4750 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P2.54mm_Vertical" V 10630 4750 50  0001 C CNN
F 3 "~" H 10700 4750 50  0001 C CNN
	1    10700 4750
	0    1    1    0   
$EndComp
$Comp
L Device:LED D1
U 1 1 61E00B67
P 10350 3100
F 0 "D1" H 10343 3317 50  0000 C CNN
F 1 "LED" H 10343 3226 50  0000 C CNN
F 2 "LED_THT:LED_D5.0mm" H 10350 3100 50  0001 C CNN
F 3 "~" H 10350 3100 50  0001 C CNN
	1    10350 3100
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR07
U 1 1 61E08C17
P 11150 2950
F 0 "#PWR07" H 11150 2800 50  0001 C CNN
F 1 "VCC" H 11165 3123 50  0000 C CNN
F 2 "" H 11150 2950 50  0001 C CNN
F 3 "" H 11150 2950 50  0001 C CNN
	1    11150 2950
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR05
U 1 1 61E0E8ED
P 9950 4500
F 0 "#PWR05" H 9950 4350 50  0001 C CNN
F 1 "VCC" H 9965 4673 50  0000 C CNN
F 2 "" H 9950 4500 50  0001 C CNN
F 3 "" H 9950 4500 50  0001 C CNN
	1    9950 4500
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR06
U 1 1 61E111B3
P 9950 4950
F 0 "#PWR06" H 9950 4700 50  0001 C CNN
F 1 "GND" H 9955 4777 50  0000 C CNN
F 2 "" H 9950 4950 50  0001 C CNN
F 3 "" H 9950 4950 50  0001 C CNN
	1    9950 4950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 3100 10200 3100
Wire Wire Line
	10500 3100 10600 3100
Wire Wire Line
	10900 3100 11150 3100
Wire Wire Line
	11150 3100 11150 2950
Wire Wire Line
	10850 4750 11100 4750
Text Label 9900 3300 0    50   ~ 0
uCtoLED
Text Label 10550 3100 3    50   ~ 0
LEDtoR
Text Label 10100 4750 0    50   ~ 0
INPUTtoR
$Comp
L power:PWR_FLAG #FLG01
U 1 1 61E176D6
P 7500 4700
F 0 "#FLG01" H 7500 4775 50  0001 C CNN
F 1 "PWR_FLAG" H 7500 4873 50  0000 C CNN
F 2 "" H 7500 4700 50  0001 C CNN
F 3 "~" H 7500 4700 50  0001 C CNN
	1    7500 4700
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG02
U 1 1 61E197E0
P 7950 4700
F 0 "#FLG02" H 7950 4775 50  0001 C CNN
F 1 "PWR_FLAG" H 7950 4873 50  0000 C CNN
F 2 "" H 7950 4700 50  0001 C CNN
F 3 "~" H 7950 4700 50  0001 C CNN
	1    7950 4700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR01
U 1 1 61E1A92E
P 7500 4800
F 0 "#PWR01" H 7500 4550 50  0001 C CNN
F 1 "GND" H 7505 4627 50  0000 C CNN
F 2 "" H 7500 4800 50  0001 C CNN
F 3 "" H 7500 4800 50  0001 C CNN
	1    7500 4800
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR02
U 1 1 61E1AFEE
P 7950 4800
F 0 "#PWR02" H 7950 4650 50  0001 C CNN
F 1 "VCC" H 7965 4973 50  0000 C CNN
F 2 "" H 7950 4800 50  0001 C CNN
F 3 "" H 7950 4800 50  0001 C CNN
	1    7950 4800
	-1   0    0    1   
$EndComp
Wire Wire Line
	7500 4700 7500 4800
Wire Wire Line
	7950 4700 7950 4800
Text Label 10900 4750 0    50   ~ 0
INPUT
Wire Wire Line
	9950 4500 9950 4650
Wire Wire Line
	9950 4650 9750 4650
Wire Wire Line
	9950 4950 9950 4850
Wire Wire Line
	9950 4850 9750 4850
Wire Wire Line
	10550 4750 9750 4750
Wire Wire Line
	9450 3550 10400 3550
Text Label 9850 3550 0    50   ~ 0
INPUT
Wire Wire Line
	6950 3150 6950 3000
$Comp
L power:VCC #PWR03
U 1 1 61E0B2DF
P 6950 3000
F 0 "#PWR03" H 6950 2850 50  0001 C CNN
F 1 "VCC" H 6965 3173 50  0000 C CNN
F 2 "" H 6950 3000 50  0001 C CNN
F 3 "" H 6950 3000 50  0001 C CNN
	1    6950 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6950 4250 6950 4400
$Comp
L power:GND #PWR04
U 1 1 61E1083F
P 6950 4400
F 0 "#PWR04" H 6950 4150 50  0001 C CNN
F 1 "GND" H 6955 4227 50  0000 C CNN
F 2 "" H 6950 4400 50  0001 C CNN
F 3 "" H 6950 4400 50  0001 C CNN
	1    6950 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 3450 9900 3100
Wire Wire Line
	9450 3450 9900 3450
NoConn ~ 9450 3650
NoConn ~ 9450 3750
NoConn ~ 9450 3850
NoConn ~ 9450 3950
NoConn ~ 4450 3950
NoConn ~ 4450 3850
NoConn ~ 4450 3750
NoConn ~ 4450 3650
NoConn ~ 4450 3550
NoConn ~ 4450 3450
$Comp
L MCU_Microchip_PIC16:PIC16F18325-ISL U1
U 1 1 61ECDDE0
P 6950 3750
F 0 "U1" H 6750 4300 50  0000 C CNN
F 1 "PIC16F18325-ISL" H 6200 4300 50  0000 C CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 6950 3150 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/devicedoc/40001795b.pdf" H 6950 3050 50  0001 C CNN
	1    6950 3750
	1    0    0    -1  
$EndComp
$Comp
L lr2:Connector J1
U 1 1 61ECA7D7
P 9350 4700
F 0 "J1" H 9400 5050 50  0000 C CNN
F 1 "Connector" H 9383 4974 50  0000 C CNN
F 2 "Connector:Banana_Jack_3Pin" H 9550 4950 50  0001 C CNN
F 3 "" H 9550 4950 50  0001 C CNN
	1    9350 4700
	1    0    0    -1  
$EndComp
$EndSCHEMATC
